package com.example.edmodo.Preference

import android.content.Context
import com.example.edmodo.R

class SplashScreenPreference(context: Context) {

    private val sharedPreference=context.getSharedPreferences(R.string.splash_screen.toString(), Context.MODE_PRIVATE)

    fun setIsFirstTime(key:String,boolean: Boolean){
        sharedPreference.edit().putBoolean(key,boolean).apply()
    }

    fun getIsFirstTime(key: String):Boolean?{return sharedPreference.getBoolean(key,true)}

}