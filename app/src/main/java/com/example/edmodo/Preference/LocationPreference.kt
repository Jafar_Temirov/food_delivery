package com.example.edmodo.Preference

import android.content.Context
import android.content.SharedPreferences
import com.example.edmodo.R

class LocationPreference(context: Context) {
    private val sharedPreference=context.getSharedPreferences(R.string.location_file.toString(), Context.MODE_PRIVATE)


    fun setLocationLat(key:String, latitude: Double) {
        sharedPreference.edit().putString(key, latitude.toString()).apply()
    }

    fun getLocationLat(key :String): String? {
        return sharedPreference.getString(key,null)
    }

    fun setLocationLong(key:String, longtitude: Double){
        sharedPreference.edit().putString(key, longtitude.toString()).apply()
    }

    fun getLocationLong(key :String): String? {
        return sharedPreference.getString(key,null)
    }

    fun setUserId(key:String,user_id:Int){
        sharedPreference.edit().putInt(key,user_id).apply()
    }
    fun getUserId(key:String):Int?{
        return sharedPreference.getInt(key,0)
    }

    fun setManzil(key:String,manzil:String){
        sharedPreference.edit().putString(key,manzil).apply()
    }
    fun getManzil(key:String):String?{
        return sharedPreference.getString(key,"null")
    }

    fun registerLocPref(context: Context,listener: SharedPreferences.OnSharedPreferenceChangeListener){
        sharedPreference.registerOnSharedPreferenceChangeListener(listener)
    }

    fun unregisterLocPref(context: Context,listener: SharedPreferences.OnSharedPreferenceChangeListener){
        sharedPreference.unregisterOnSharedPreferenceChangeListener(listener)
    }
}