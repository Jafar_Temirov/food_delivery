package com.example.edmodo.Preference

import android.content.Context
import com.example.edmodo.R

class RestaurantPreference(context: Context) {
    private val sharedPreference=context.getSharedPreferences(R.string.restaurant_file.toString(),Context.MODE_PRIVATE)

    fun setRestaurantRealName(key:String, name: String) {
        sharedPreference.edit().putString(key,name).apply()
    }

    fun getRestaurantRealName(key :String): String? {
        return sharedPreference.getString(key,null)
    }

    fun setRestaurantRealImage(key:String, uri: String) {
        sharedPreference.edit().putString(key,uri).apply()
    }

    fun getRestaurantRealImage(key :String): String? {
        return sharedPreference.getString(key,null)
    }

    fun setRestaurantRealId(key:String, id: Int) {
        sharedPreference.edit().putInt(key,id).apply()
    }

    fun getRestaurantRealId(key :String): Int? {
        return sharedPreference.getInt(key,0)}

    fun setDistance(key:String,distance:String){
        sharedPreference.edit().putString(key,distance).apply()
    }
    fun getDistance(key:String):String?{return sharedPreference.getString(key,null)}

}