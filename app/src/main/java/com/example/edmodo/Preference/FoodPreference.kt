package com.example.edmodo.Preference

import android.content.Context
import android.content.SharedPreferences
import com.example.edmodo.R

class FoodPreference(context: Context) {
    private val sharedPreference=context.getSharedPreferences(R.string.second_file.toString(), Context.MODE_PRIVATE)

    fun setExistFood(key:String,boolean: Boolean){
        sharedPreference.edit().putBoolean(key,boolean).apply()
    }
    fun getExistFood(key: String):Boolean?{return sharedPreference.getBoolean(key,false) }

    fun setWholeMoney(key:String,money:Int){
        sharedPreference.edit().putString(key, money.toString()).apply()
    }
    fun getWholeMoney(key:String):String?{return sharedPreference.getString(key,null) }

    fun setCounter(key: String,count:Int){
       sharedPreference.edit().putInt(key,count).apply() }
    fun getCounter(key:String):Int?{return sharedPreference.getInt(key,0)}

    fun registerPref(context: Context,listener: SharedPreferences.OnSharedPreferenceChangeListener){
        sharedPreference.registerOnSharedPreferenceChangeListener(listener)
    }

    fun unregisterPref(context: Context,listener: SharedPreferences.OnSharedPreferenceChangeListener){
        sharedPreference.unregisterOnSharedPreferenceChangeListener(listener)
    }

    fun setRestaurantId(key: String, id: Int){
        sharedPreference.edit().putInt(key,id).apply()
    }
    fun getRestaurantId(key:String):Int?{return sharedPreference.getInt(key,0)}

    fun setRestaurant_ImageUri(key:String,uri:String){
        sharedPreference.edit().putString(key,uri).apply()
    }
    fun getRestaurant_ImageUri(key:String):String?{return sharedPreference.getString(key,null)}

    fun setDistance(key:String,distance:String){
        sharedPreference.edit().putString(key,distance).apply()
    }
    fun getDistance(key:String):String?{return sharedPreference.getString(key,null)}

    fun setRestaurantName(key:String,name:String){
        sharedPreference.edit().putString(key,name).apply()
    }
    fun getRestaurantName(key:String) :String?{return sharedPreference.getString(key,null)}

}