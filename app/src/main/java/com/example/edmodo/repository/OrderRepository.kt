package com.example.edmodo.repository

import com.example.edmodo.Api.ApiClient
import com.example.edmodo.Api.ApiService

class OrderRepository {
    private var Api: ApiService = ApiClient.createService(ApiService::class.java)

    suspend fun getUnOrder(user_id: Int,confirm_id:Int)=Api.getUnconfirmed(user_id,confirm_id)

}