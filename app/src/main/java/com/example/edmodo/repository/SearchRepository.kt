package com.example.edmodo.repository

import com.example.edmodo.Api.ApiClient
import com.example.edmodo.Api.ApiService
import com.example.edmodo.database.FoodDatabase
import com.google.android.gms.common.api.Api

class SearchRepository(val db: FoodDatabase) {
    private var Api: ApiService = ApiClient.createService(ApiService::class.java)
    suspend fun seachFood(key:String)=Api.searchFood(key)
    suspend fun getFoodType()= Api.getFoodType()
}