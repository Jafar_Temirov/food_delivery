package com.example.edmodo.repository
import com.example.edmodo.Api.ApiClient
import com.example.edmodo.Api.ApiService
import com.example.edmodo.database.FoodDatabase
import com.example.edmodo.database.entity.FoodEntity

class RestaurantsRepository(val db:FoodDatabase){
    private var Api:ApiService=ApiClient.createService(ApiService::class.java)
    // Api
    suspend fun getAllRestaurants()=Api.getRestaurants()
    suspend fun getMenu(menu_id:Int)=Api.getMenus(menu_id)
    suspend fun getFood(id:Int)=Api.getFoods(id)
    suspend fun saveUser(username:String,surname:String,location:String,tel_number:String, lat:String,long:String)=
        Api.saveUser(username,surname,location,tel_number,lat,long)
    suspend fun UpdateLocation(lat:String,long: String,user_id:Int,manzil:String)=
        Api.updateLocation(lat,long,user_id,manzil)
    suspend fun getUnOrder(user_id: Int,confirm_id:Int)=Api.getUnconfirmed(user_id,confirm_id)

    suspend fun SaveOrders(user_id:Int,
                           restaurant_name:String,
                           restaurant_id:Int,
                           umumiy_narx:String,
                           ovqat_narxi:String,
                           yetkazish_narxi:String,
                           ovqatlar:String,
                           manzil:String,
                           qoshimcha_raqam:String,
                           yetkazish_vaqt:String
    )=Api.SaveOrder(user_id,restaurant_name,restaurant_id,umumiy_narx,ovqat_narxi,yetkazish_narxi,ovqatlar,manzil,qoshimcha_raqam,yetkazish_vaqt)


    // Database
    fun getFoodsDb()=db.getFoodsDao().getAllFoods()

    suspend fun deleteFood(food: FoodEntity)=db.getFoodsDao().deleteFood(food)

    suspend fun insert(food: FoodEntity)=db.getFoodsDao().insert(food)

    fun comparing(dd:Int)=db.getFoodsDao().compareFood(dd)

    suspend fun UpdateCounter(count:Int,food_id:Int)=db.getFoodsDao().update(count,food_id)

    suspend fun deleteAll()=db.getFoodsDao().deleteAll()

    fun getMoney()=db.getFoodsDao().getSumm()

    suspend fun updateDefault(food: FoodEntity)=db.getFoodsDao().updateDefault(food)

}