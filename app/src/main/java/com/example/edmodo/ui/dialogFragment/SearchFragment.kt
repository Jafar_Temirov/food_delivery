package com.example.edmodo.ui.dialogFragment

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.edmodo.R
import com.example.edmodo.util.Constants
import com.example.edmodo.Preference.FoodPreference
import com.example.edmodo.adapters.FoodTypeAdapter
import com.example.edmodo.adapters.searchadapter.SearchAdapter
import com.example.edmodo.database.FoodDatabase
import com.example.edmodo.model.Food
import com.example.edmodo.model.FoodType
import com.example.edmodo.repository.SearchRepository
import com.example.edmodo.ui.fragment.SavatchaFragment
import com.example.edmodo.util.Resource
import com.example.edmodo.viewModel.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SearchFragment : DialogFragment(){

    lateinit var viewModel: SearchViewModel
    lateinit var foodTypeAdapter:FoodTypeAdapter
    lateinit var foodAdapter: SearchAdapter
    private  var LayoutManager: RecyclerView.LayoutManager? = null

    private var searchLayoutManager: RecyclerView.LayoutManager? = null
    lateinit var searchrecyclerView: RecyclerView

    lateinit var foodPreference: FoodPreference
    lateinit var recyclerViewMostView: RecyclerView

    var progressbar: ProgressBar?=null
    lateinit var searchview:EditText
    lateinit var not_data:TextView
    val TAG="SearchFragment"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setStyle(STYLE_NORMAL, R.style.FullscreenDialogtheme)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view:View=inflater.inflate(R.layout.fragment_search, container, false)
        val searchRepository= SearchRepository(FoodDatabase(requireActivity()))
        val viewmodelprovderFactory= SearchViewModelProviderFactory(requireActivity().application, searchRepository)
        viewModel= ViewModelProvider(requireActivity(), viewmodelprovderFactory).get(SearchViewModel::class.java)
        progressbar=view.findViewById(R.id.search_progressBar)
        foodPreference= FoodPreference(requireActivity())
        not_data=view.findViewById(R.id.not_data)
        searchrecyclerView=view.findViewById(R.id.recommended_recycler)
        recyclerViewMostView=view.findViewById(R.id.most_viewed_recycler)
        searchLayoutManager=LinearLayoutManager(activity)
        LayoutManager=LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        recyclerViewMostView.apply {
            layoutManager=LayoutManager
            itemAnimator= DefaultItemAnimator()
            isNestedScrollingEnabled=false
            setHasFixedSize(true)
        }
        searchrecyclerView.apply {
            layoutManager=searchLayoutManager
            itemAnimator= DefaultItemAnimator()
            isNestedScrollingEnabled=false
            setHasFixedSize(true)
        }

        searchview=view.findViewById(R.id.editText)
        view.findViewById<ImageView>(R.id.imageViewss).setOnClickListener {dismiss()}
        fetchMostViewed()

        var job: Job?=null
        searchview.addTextChangedListener {editable->
            job?.cancel()
            job= MainScope().launch {
                delay(1000L)
                editable?.let {
                    if (editable.toString().isNotEmpty()){
                        viewModel.searchFoods(editable.toString().trim())
                        closeKeyboard()
                    }
                }
            }
        }
        if (searchview.text.toString().trim().isEmpty()){
            viewModel.searchFoods("")
        }
        viewModel.search.observe(viewLifecycleOwner, Observer {response->
            when(response){
                is Resource.Success->{
                    hideProgressbar()
                    response.data?.let {
                        Log.d(TAG, "Observe $it")

                        seachViewRecycler(it)
                        if(foodPreference.getExistFood(Constants.EXIST_FOOD)!!) {
                            val dostavka = view.findViewById<RelativeLayout>(R.id.mainactivity_dostavka)
                            val cc = foodPreference.getCounter(Constants.COUNTER)
                            val mainactivity_dostavka_counter=view.findViewById<TextView>(R.id.mainactivity_dostavka_counter)
                            mainactivity_dostavka_counter.text = cc.toString()
                            dostavka.visibility = View.VISIBLE
                            dostavka.setOnClickListener {
                                dismiss()
                                val fragment: Fragment = SavatchaFragment()
                                requireActivity().supportFragmentManager
                                    .beginTransaction()
                                    .replace(R.id.fragment_container, fragment)
                                    .commit()
                            }

                        }
                    }

                }
                is Resource.Error->{
                    hideProgressbar()
                    response.message?.let { message->
                        Log.d(TAG,"Error : $message")
                        Toast.makeText(activity,"An error occured: $message", Toast.LENGTH_LONG).show()
                    }
                }
                is Resource.Loading->{
                    showProgressbar()
                }
            }
        })

        return view
    }
    private fun fetchMostViewed(){
        viewModel.getFoodType()
        viewModel.foodType.observe(viewLifecycleOwner, Observer {response->
            when(response){
                is Resource.Success->{
                    hideProgressbar()
                    response.data?.let {
                        Log.d(TAG, it.toString())
                        mostViewedRecycler(it)
                    }

                }
                is Resource.Error->{
                    hideProgressbar()
                    response.message?.let { message->
                        Log.d(TAG,"Error : $message")
                        Toast.makeText(activity,"An error occured: $message", Toast.LENGTH_LONG).show()
                    }
                }
                is Resource.Loading->{
                    showProgressbar()
                }
            }
        })
    }

    private fun mostViewedRecycler(list: List<FoodType>) {
        if (list.size<=0){
            not_data.visibility=View.VISIBLE
        }else {
            not_data.visibility=View.GONE
            Log.e(TAG,"Size of search ${list.size}" )
        }
        foodTypeAdapter= FoodTypeAdapter(requireActivity(),list)
        recyclerViewMostView.adapter = foodTypeAdapter
        foodTypeAdapter.notifyDataSetChanged()
    }
    private fun seachViewRecycler(list: List<Food>){
        foodAdapter= SearchAdapter(
            requireActivity(),
            list
        )
        searchrecyclerView.adapter=foodAdapter
        foodAdapter.notifyDataSetChanged()
    }


    override fun onActivityCreated(arg0: Bundle?) {
        super.onActivityCreated(arg0)
        dialog!!.window!!.attributes.windowAnimations = R.style.DialogAnimation
    }

    private fun hideProgressbar() {
        progressbar!!.visibility = View.GONE
   }
    private fun showProgressbar(){
        progressbar!!.visibility=View.INVISIBLE
    }

    private fun hideKeyboard(activity: Activity) {
        val imm: InputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
    private fun closeKeyboard(){
        val view: View? =requireActivity().currentFocus
        val imm = requireActivity().getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager
        //imm.hideSoftInputFromWindow(view!!.windowToken, 0)
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
    }
}