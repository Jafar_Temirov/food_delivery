package com.example.edmodo.ui.activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Html
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.chaos.view.PinView
import com.example.edmodo.R
import com.example.edmodo.util.Function
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.tasks.TaskExecutors
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.auth.PhoneAuthProvider.ForceResendingToken
import com.google.firebase.auth.PhoneAuthProvider.OnVerificationStateChangedCallbacks
import kotlinx.android.synthetic.main.activity_vertification_code.*
import java.util.concurrent.TimeUnit
class VertificationCodeActivity : AppCompatActivity() {

    val ERROR_DIALOG_REQUEST=9001
    lateinit var pinView:PinView
    lateinit var relativeLayout:RelativeLayout
    lateinit var sms_code_time:TextView
    var condition:Boolean=true
    lateinit var codeBySystem:String
    val TAG="VertificationCode"
    lateinit var number:String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_vertification_code)

        val sms_code_title=findViewById<TextView>(R.id.sms_code_title)
        val once_time=findViewById<TextView>(R.id.once_time)
        val title_of_number=findViewById<TextView>(R.id.title_of_number)
        val vertification_code_btn=findViewById<TextView>(R.id.vertification_code_btn)

        exit.setOnClickListener { finish() }

        number= intent.getStringExtra("number").toString()
        sms_code_time=findViewById(R.id.sms_code_time)
        pinView=findViewById(R.id.pin_view)
        relativeLayout=findViewById(R.id.relativelayout_code)
        val text= "<br><font color='#1c1e33'>$number</font>"
        title_of_number.text=Html.fromHtml(getString(R.string.title_of_verifycode)+text)

        sendVertificationCodetoUser(number)
        hideSoftKeyboard()
        vertification_code_btn.setOnClickListener {
            if (!pinView.text.isNullOrEmpty()){
                 verifyCode(pinView.text.toString())
            }else{
                Function.showToast(this,"Code ni kiritishiz shart")
            }
        }
        once_time.setOnClickListener {
            once_time.visibility=View.GONE
            sms_code_title.visibility=View.GONE
            sms_code_time.visibility=View.VISIBLE
            countDown()
            sendVertificationCodetoUser(number)
        }
        countDown()
    }
    private fun countDown(){
        object : CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                sms_code_time.text="00:"+millisUntilFinished/1000
            }
            override fun onFinish() {
                Log.e("Jafar:::","done")
                sms_code_time.visibility=View.GONE
                once_time.visibility=View.VISIBLE
                sms_code_title.visibility=View.VISIBLE
                condition=false
            }
        }.start()
    }

    private fun sendVertificationCodetoUser(number:String){
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
            number, // Phone number to verify
            60, // Timeout duration
            TimeUnit.SECONDS, // Unit of timeout
            TaskExecutors.MAIN_THREAD, // Activity (for callback binding)
            callbacks) // OnVerificationStateChangedCallbacks
    }
    private val callbacks: OnVerificationStateChangedCallbacks = object : OnVerificationStateChangedCallbacks() {
            override fun onCodeSent(s: String, forceResendingToken: ForceResendingToken) {
                super.onCodeSent(s, forceResendingToken)
                codeBySystem=s
            }

            override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {
                val codes = phoneAuthCredential.smsCode
                if (codes != null) {
                  pinView.setText(codes)
                  verifyCode(codes)
                  sms_code_time.visibility=View.INVISIBLE
                }
            }
            override fun onVerificationFailed(e: FirebaseException) {
               Function.showToast(this@VertificationCodeActivity,e.localizedMessage!!)
            }
        }

    fun verifyCode(code:String){
        val credential:PhoneAuthCredential= PhoneAuthProvider.getCredential(codeBySystem,code)
        SignInUsingCredential(credential)
    }

    private fun SignInUsingCredential(credential: PhoneAuthCredential) {
        val firebaseAuth:FirebaseAuth=FirebaseAuth.getInstance()
        firebaseAuth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    if (isServicesOk()) {
                        Log.d(TAG, "Verification Completed")
                        val intent = Intent(this, MapActivity::class.java)
                        intent.putExtra("tel_number", number)
                        startActivity(intent)
                        finish()
                    }else Function.showToast(this,"Ruxsat berilmagan !!!")
                } else {
                    if (task.exception is FirebaseAuthInvalidCredentialsException) {
                     Function.showToast(this,"Vertification not completed , Try again")
                    }
                }
            }
    }

    private fun hideSoftKeyboard(){this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN) }

    override fun onBackPressed() {
        if (condition){
            Function.snackbar(relativeLayout,"Sms-kod kelguncha kutib turing !!!")
        }else  super.onBackPressed()
    }

    private fun isServicesOk() : Boolean {
        Log.d(TAG,"isServicesOk true")
        val available= GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this)
        when {
            available== ConnectionResult.SUCCESS -> {
                Log.d(TAG,"isServicesOk true Google Play Servies")
                return true
            }
            GoogleApiAvailability.getInstance().isUserResolvableError(available) -> {
                Log.d(TAG,"isServicesOk true Error")
                val dialog: Dialog = GoogleApiAvailability.getInstance().getErrorDialog(this,available,ERROR_DIALOG_REQUEST)
                dialog.show()
            }
            else -> {
                Function.showToast(this,"No can't make map request")
            }
        }
        return false
    }
}