package com.example.edmodo.ui.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.edmodo.R
import com.example.edmodo.ui.MainActivity
import com.example.edmodo.util.Constants.Companion.ISFIRST
import com.example.edmodo.Preference.SplashScreenPreference

class SplashScreenActivity : AppCompatActivity() {
    private val SPLASH_SCREEN =2500
    var topAnim: Animation? = null
    var bottomAnim:Animation? = null
    var leftAnim:Animation?=null
    lateinit var slogan: TextView
    lateinit var desc:TextView
    lateinit var splashScreenPreference: SplashScreenPreference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_splash_screen)

        splashScreenPreference= SplashScreenPreference(this)
        slogan=findViewById(R.id.splash_welcome)
        desc=findViewById(R.id.splash_food_desc)
        topAnim=AnimationUtils.loadAnimation(this,R.anim.top_animation)
        bottomAnim=AnimationUtils.loadAnimation(this,R.anim.bottom_animation)
        leftAnim=AnimationUtils.loadAnimation(this,R.anim.bottom_animation)
        slogan.animation=bottomAnim
        desc.animation=leftAnim

        Handler().postDelayed({ // Attach all the elements those you want to animate in design
            val isFirsttime = splashScreenPreference.getIsFirstTime(ISFIRST)
            if (isFirsttime!!) {
                val intent = Intent(this@SplashScreenActivity, NumberVertificationActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                val intent = Intent(this@SplashScreenActivity, MainActivity::class.java)
                startActivity(intent)
                finish()
            }
        },SPLASH_SCREEN.toLong())

    }

    override fun onBackPressed() {}
}