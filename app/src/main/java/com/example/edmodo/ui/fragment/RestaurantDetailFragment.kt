package com.example.edmodo.ui.fragment
import android.content.ContentValues
import android.content.SharedPreferences
import android.graphics.drawable.Drawable
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.example.edmodo.R
import com.example.edmodo.adapters.AdapterPager
import com.example.edmodo.database.FoodDatabase
import com.example.edmodo.model.RestaurantMenu
import com.example.edmodo.repository.RestaurantsRepository
import com.example.edmodo.util.Constants
import com.example.edmodo.util.Constants.Companion.COUNTER
import com.example.edmodo.util.Constants.Companion.DISTANCE
import com.example.edmodo.util.Constants.Companion.EXIST_FOOD
import com.example.edmodo.util.Constants.Companion.MONEY
import com.example.edmodo.util.Constants.Companion.RESTAURANT_ID
import com.example.edmodo.util.Constants.Companion.RESTAURANT_IMAGE_URI
import com.example.edmodo.util.Constants.Companion.RESTAURANT_NAME
import com.example.edmodo.Preference.FoodPreference
import com.example.edmodo.Preference.LocationPreference
import com.example.edmodo.util.Resource
import com.example.edmodo.viewModel.RestaurantDetailViewModel
import com.example.edmodo.viewModel.RestaurantDetailViewModelFactory
import com.google.android.material.tabs.TabLayout
import java.text.DecimalFormat
import kotlin.collections.ArrayList

class RestaurantDetailFragment : DialogFragment(),SharedPreferences.OnSharedPreferenceChangeListener {
    lateinit var viewModel: RestaurantDetailViewModel
    lateinit var foodpreference: FoodPreference
    var TAG="RestaurantDetailFRagment"
    lateinit var tabLayout:TabLayout
    lateinit var viewpager:ViewPager
    private var mTitleList = ArrayList<String>()
    lateinit var menular:List<RestaurantMenu>
    private var mFragmentList = ArrayList<Fragment>()
    private var res_id= ArrayList<Int>()
    private lateinit var pagerAdapter: AdapterPager
    private var back_elonfragment:ImageView?=null
    private var ishdetail_love:ImageView?=null
    var res_name:String?=null
    var res_rating:String?=null
    var latitude:String?=null
    var longtitude:String?=null
    var image_uri:String?=null
    var restaurant_id:Int?=null
    var more_info:String?=null
    lateinit var uri:ImageView
    private lateinit var prefernce: LocationPreference
    var dostavka:RelativeLayout?=null
    var money:TextView?=null
    var restaurantdetail_dostavka_counter:TextView?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setStyle(STYLE_NORMAL, R.style.FullscreenDialogtheme)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view:View= inflater.inflate(R.layout.fragment_restaurant_detail, container, false)
        prefernce= LocationPreference(requireActivity())
        foodpreference= FoodPreference(requireActivity())
        if(arguments!=null){
            res_name = requireArguments().getString("restaurant_name")
            more_info=requireArguments().getString("restaurant_more_info")
            image_uri=requireArguments().getString("restaurant_main_image")
            res_rating=requireArguments().getString("restaurant_rating")
            restaurant_id=requireArguments().getInt("restaurant_id")
            longtitude=requireArguments().getString("restaurant_longtitude")
            latitude=requireArguments().getString("restaurant_latitude")
        }

         dostavka=view.findViewById(R.id.restaurantdetail_dostavka)
         money= view.findViewById(R.id.restaurantdetail_overallmoney)
         restaurantdetail_dostavka_counter= view.findViewById(R.id.restaurantdetail_dostavka_counter)

        Log.d(TAG,"RESTAURANT ID $restaurant_id")
        val repository=RestaurantsRepository(FoodDatabase(requireActivity()))
        val viewModelFactory=RestaurantDetailViewModelFactory(requireActivity().application,repository)
        viewModel=ViewModelProvider(requireActivity(),viewModelFactory).get(RestaurantDetailViewModel::class.java)
        restaurant_id?.let { viewModel.getMenu(it) }
        viewModel.menus.observe(viewLifecycleOwner, Observer { response->
            when(response){
                is Resource.Success->{
                    hideProgressbar()
                    response.data?.let {
                        menular=it
                        mTitleList.clear()
                        res_id.clear()
                        mFragmentList.clear()
                        for (i in menular){
                            Log.d(TAG,"Response::"+i.id.toString()+i.menu)
                            mTitleList.add(i.menu)
                            res_id.add(i.id)
                        }
                        initView(view)
                    }
                }
                is Resource.Error->{
                    hideProgressbar()
                    response.message?.let {message->
                        Log.d(TAG,"Error : $message")
                        Toast.makeText(activity,"An error occured: $message",Toast.LENGTH_LONG).show()
                    }
                }
                is Resource.Loading->{
                    showProgressbar()
                }
            }
        })

        uri=view.findViewById(R.id.restaurant_detail_uri)
        back_elonfragment=view.findViewById(R.id.back_restaurant_detal)
        ishdetail_love=view.findViewById(R.id.restaurant_detail_love)
        back_elonfragment!!.setOnClickListener {dismiss()}
        ishdetail_love!!.setOnClickListener { Toast.makeText(activity,"Love",Toast.LENGTH_SHORT).show() }

        val myR = view.findViewById<TextView>(R.id.restaurant_detail_title)
        val food_arriving_time = view.findViewById<TextView>(R.id.restdetail_arriving_time)
        val food_price = view.findViewById<TextView>(R.id.restdetailfood_price)
        val food_rating = view.findViewById<TextView>(R.id.restaurant_detail_rating)
        val more_information=view.findViewById<TextView>(R.id.restaurant_detail_info)
        myR.text=res_name
        more_information.text=more_info
        food_rating.text=res_rating+"(0)"
        var distance: Float?=null
        var cost_arriving:Double?=null
        var dat:String?=null
        var nomer:Int?=null
        var prefgadata:String?=null
        if (prefernce!=null){
            val locationA = Location("point A")
            locationA.latitude = latitude!!.toDouble()
            locationA.longitude =longtitude!!.toDouble()
            val locationB = Location("point B")
            locationB.latitude = prefernce.getLocationLat(Constants.LATITUDE)!!.toDouble()
            locationB.longitude = prefernce.getLocationLat(Constants.LONGTITUDE)!!.toDouble()
            distance= locationA.distanceTo(locationB)
            Log.d("TAG", "Adapter Jafar $distance")
            distance /= 1000
            cost_arriving=Math.round(distance * 10.0) / 10.0
            prefgadata= cost_arriving.toString()
            dat=cost_arriving.toString()+"km"
            cost_arriving *= 1500
            nomer= cost_arriving.toInt()
            val formatter = DecimalFormat("#,###,###")
            val ss: Number? =formatter.parse(nomer.toString())
            food_arriving_time.text=dat
            food_price.text=ss.toString()+"so'm"
        }

        prefgadata?.let { foodpreference.setDistance(DISTANCE,it)}
        image_uri?.let { foodpreference.setRestaurant_ImageUri(RESTAURANT_IMAGE_URI,it)}
        restaurant_id?.let { foodpreference.setRestaurantId(RESTAURANT_ID,it)}
        res_name?.let { foodpreference.setRestaurantName(RESTAURANT_NAME,it)}
        val requestOptions = RequestOptions()
            .placeholder(R.color.card2)
            .error(R.color.red_100)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
        Glide.with(requireActivity())
            .applyDefaultRequestOptions(requestOptions)
            .load(Uri.parse(image_uri))
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(p0: GlideException?, p1: Any?, p2: Target<Drawable>?, p3: Boolean): Boolean {
                   // holder.progressbar.visibility = View.GONE
                    return false
                }
                override fun onResourceReady(p0: Drawable?, p1: Any?, p2: Target<Drawable>?, p3: DataSource?, p4: Boolean): Boolean {
                    Log.d(ContentValues.TAG, "OnResourceReady")
                    //holder.progressbar.setVisibility(View.GONE)
                    return false
                }
            })
            //.transition(DrawableTransitionOptions.withCrossFade())
            .into(uri)
        // It will work even SharedPreference is not changed
        if (foodpreference.getExistFood(EXIST_FOOD)!!){
            dostavka?.visibility=View.VISIBLE
            money!!.text=foodpreference.getWholeMoney(MONEY)
            restaurantdetail_dostavka_counter?.text= foodpreference.getCounter(COUNTER).toString()

            dostavka!!.setOnClickListener {
               // dismiss()
                val fragment: Fragment = SavatchaFragment()
                requireActivity().supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit()
            }
        }


        return view
    }

    override fun onSharedPreferenceChanged(p0: SharedPreferences?, p1: String?) {
        if (foodpreference.getExistFood(EXIST_FOOD)!!){
            dostavka?.visibility=View.VISIBLE

            if (p1.equals(COUNTER)){
                money!!.text=foodpreference.getWholeMoney(MONEY)
                restaurantdetail_dostavka_counter?.text= foodpreference.getCounter(COUNTER).toString()
            }
        }

        dostavka!!.setOnClickListener {
            dismiss()
            val fragment: Fragment = SavatchaFragment()
            requireActivity().supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit()
        }

    }

    override fun onResume() {
        super.onResume()
        foodpreference.registerPref(requireActivity(),this)
    }
    override fun onPause() {
        foodpreference.unregisterPref(requireActivity(),this)
        super.onPause()
    }

    private fun initView(view:View) {
        tabLayout = view.findViewById(R.id.tablayout_resdetail)
        viewpager = view.findViewById(R.id.view_pager_restdetail)
        pagerAdapter= AdapterPager(childFragmentManager,res_id,mTitleList,mFragmentList)
        setUpViewPager(viewpager)
        tabLayout.setupWithViewPager(viewpager)
        viewpager.setOnClickListener {
          return@setOnClickListener
        }
    }

    private fun setUpViewPager(viewpager:ViewPager){
        Log.d(TAG,mTitleList.size.toString()+"size")
        pagerAdapter.notifyDataSetChanged()
        viewpager.adapter = pagerAdapter
    }

    private fun hideProgressbar(){
     Log.d(TAG,"hideProgress")
    }

    private fun showProgressbar(){
        Log.d(TAG,"showProgress")
    }

    override fun onActivityCreated(arg0: Bundle?) {
        super.onActivityCreated(arg0)
        dialog!!.window!!.attributes.windowAnimations = R.style.DialogAnimation}
}


