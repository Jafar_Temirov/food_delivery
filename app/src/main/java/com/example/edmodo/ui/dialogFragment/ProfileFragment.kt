package com.example.edmodo.ui.dialogFragment

import android.content.ContentValues.TAG
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.example.edmodo.Preference.FoodPreference
import com.example.edmodo.Preference.LocationPreference
import com.example.edmodo.Preference.RestaurantPreference
import com.example.edmodo.Preference.SplashScreenPreference
import com.example.edmodo.R
import com.example.edmodo.database.FoodDatabase
import com.example.edmodo.repository.RestaurantsRepository
import com.example.edmodo.util.Constants.Companion.ISFIRST
import com.example.edmodo.util.Function
import com.example.edmodo.viewModel.RestaurantDetailViewModel
import com.example.edmodo.viewModel.RestaurantDetailViewModelFactory
import com.google.android.material.bottomsheet.BottomSheetDialog
import org.w3c.dom.Text
//  Bu qismda biz yangi bolim , Murojaatlar, fikrlar degan bo'lim xam ochamiz

class ProfileFragment : DialogFragment() {
    lateinit var viewmodel: RestaurantDetailViewModel
    lateinit var profile_vixod:TextView
    lateinit var splashScreenPreference: SplashScreenPreference
    lateinit var restaurantPreference: RestaurantPreference
    lateinit var locationPreference: LocationPreference
    lateinit var foodPreference: FoodPreference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setStyle(STYLE_NORMAL, R.style.FullscreenDialogtheme)
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view:View=inflater.inflate(R.layout.fragment_profile, container, false)

        val newsRepository= RestaurantsRepository(FoodDatabase(requireActivity()))
        val viewmodelprovderFactory= RestaurantDetailViewModelFactory(
            requireActivity().application,
            newsRepository
        )
        viewmodel= ViewModelProvider(requireActivity(), viewmodelprovderFactory).get(
            RestaurantDetailViewModel::class.java
        )

        profile_vixod=view.findViewById(R.id.profile_vixod)
        splashScreenPreference= SplashScreenPreference(requireActivity())
        restaurantPreference= RestaurantPreference(requireActivity())
        locationPreference= LocationPreference(requireActivity())
        foodPreference=FoodPreference(requireActivity())

        profile_vixod.setOnClickListener {
            val bottomSheet = BottomSheetDialog(requireActivity(), R.style.BottomsheetDemo)
            val view1 = LayoutInflater.from(requireActivity())
                .inflate(
                    R.layout.layout_bottom_sheet,
                    view.findViewById<View>(R.id.bottomSheetContainer) as LinearLayout?
                )
            view1.findViewById<View>(R.id.btn_dismiss).setOnClickListener {
                bottomSheet.dismiss()
            }

            view1.findViewById<View>(R.id.btn_close).setOnClickListener {
                splashScreenPreference.setIsFirstTime(ISFIRST, true)
                val preferences: SharedPreferences =
                    requireActivity().getSharedPreferences(
                        R.string.restaurant_file.toString(),
                        0
                    )
                preferences.edit().clear().apply()
                val preferences2: SharedPreferences =
                    requireActivity().getSharedPreferences(
                        R.string.second_file.toString(),
                        0
                    )
                preferences2.edit().clear().apply()
                val preferences3: SharedPreferences =
                    requireActivity().getSharedPreferences(
                        R.string.location_file.toString(),
                        0
                    )
                preferences3.edit().clear().apply()
                viewmodel.deleteAllOreders()
                Function.showToast(requireActivity(),"Log out")
                requireActivity().finish()

            }
            bottomSheet.setContentView(view1)
            bottomSheet.show()
        }
        view.findViewById<ImageView>(R.id.meening_profileback).setOnClickListener { dismiss() }

        view.findViewById<TextView>(R.id.profile_username).setOnClickListener {
            Log.e(TAG, "clikc")
            val dial: DialogFragment =ChangeUsername()
            dial.show(childFragmentManager, "manzil")
        }

        view.findViewById<TextView>(R.id.profile_phonenumber).setOnClickListener {
            val dial1: DialogFragment =ChangePhoneNumber()
            dial1.show(childFragmentManager, "manzil")
            Log.e(TAG, "clikc2")
        }
        view.findViewById<TextView>(R.id.profile_dastur_haqida).setOnClickListener {
            val dial3: DialogFragment =DasturHaqida()
            dial3.show(childFragmentManager, "manzil")
        }
        view.findViewById<TextView>(R.id.profile_language).setOnClickListener {
            Function.showToast(requireActivity(),"Malumot hozircha yo'q.!!!")
        }
        view.findViewById<TextView>(R.id.profile_kontakti).setOnClickListener {
            val bottomSheet2 = BottomSheetDialog(requireActivity(), R.style.BottomsheetDemo)
            val view1 = LayoutInflater.from(requireActivity())
                .inflate(
                    R.layout.layout_kontakt_bottomsheet,
                    view.findViewById<View>(R.id.bottomSheetContainer2) as LinearLayout?
                )
            view1.findViewById<View>(R.id.btn_aloqa).setOnClickListener {
                val intent = Intent(Intent.ACTION_DIAL)
                intent.data = Uri.parse("tel:+998906108036")
                startActivity(intent)
            }
            bottomSheet2.setContentView(view1)
            bottomSheet2.show()
        }
        view.findViewById<TextView>(R.id.favourite_title).setOnClickListener {
            Function.showToast(requireActivity(),"Malumot hozircha yo'q.!!!")
        }
        return view
    }

    override fun onActivityCreated(arg0: Bundle?) {
        super.onActivityCreated(arg0)
        dialog!!.window!!.attributes.windowAnimations = R.style.DialogAnimation
    }
}