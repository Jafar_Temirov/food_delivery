package com.example.edmodo.ui

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.edmodo.R
import com.example.edmodo.database.FoodDatabase
import com.example.edmodo.repository.RestaurantsRepository
import com.example.edmodo.ui.fragment.HomeFragment
import com.example.edmodo.ui.fragment.SavatchaFragment
import com.example.edmodo.ui.fragment.orderfragment.OrdersFragment
import com.example.edmodo.util.Constants.Companion.COUNTER
import com.example.edmodo.util.Constants.Companion.EXIST_FOOD
import com.example.edmodo.Preference.FoodPreference
import com.example.edmodo.Preference.LocationPreference
import com.example.edmodo.viewModel.RestaurantsViewModel
import com.example.edmodo.viewModel.RestaurantsViewModelProviderFactory
import com.ismaeldivita.chipnavigation.ChipNavigationBar

class MainActivity : AppCompatActivity(){
    lateinit var chipNavigationBar: ChipNavigationBar
    lateinit var viewModel: RestaurantsViewModel
    private lateinit var locationpref: LocationPreference
    private val menu by lazy { findViewById<ChipNavigationBar>(R.id.bottom_navigation) }
    lateinit var foodPreference: FoodPreference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_main)

        locationpref= LocationPreference(applicationContext)
        foodPreference= FoodPreference(applicationContext)
        getDeviceLocation()

        val newsRepository=RestaurantsRepository(FoodDatabase(this))
        val viewmodelprovderFactory=RestaurantsViewModelProviderFactory(application,newsRepository)
        viewModel=ViewModelProvider(this,viewmodelprovderFactory).get(RestaurantsViewModel::class.java)
        chipNavigationBar = findViewById(R.id.bottom_navigation)

        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, HomeFragment()).commit()
        chipNavigationBar.setOnItemSelectedListener(object :
            ChipNavigationBar.OnItemSelectedListener {
            override fun onItemSelected(id: Int) {
                var fragment: Fragment? = null
                when (id) {
                    R.id.homes -> fragment = HomeFragment()
                    R.id.orders -> fragment =OrdersFragment()
                    R.id.boxes -> fragment = SavatchaFragment()
                }
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment!!)
                    .commit()
            }
        })

        // Navigation bar set up
    /*    chipNavigationBar.setOnApplyWindowInsetsListener { view, windowInsets ->
            view.updatePadding(bottom = windowInsets.systemWindowInsetBottom)
            windowInsets
        }

     */

       /* viewModel.text.observe(this, Observer {char->
            if (savedInstanceState==null) {
                menu.setItemSelected(R.id.homes)
                menu.showBadge(R.id.boxes, char.toString().toInt())
            }
        }   )*/

        if(foodPreference.getExistFood(EXIST_FOOD)!!){
            val cc=foodPreference.getCounter(COUNTER)
            if (cc != null) {
              // I should show data here
              //  menu.showBadge(R.id.boxes, cc)
            }
        }
    }

    private fun getDeviceLocation(){
            if (ContextCompat.checkSelfPermission(applicationContext,
                    Manifest.permission.ACCESS_FINE_LOCATION)+
                ContextCompat.checkSelfPermission(applicationContext,
                    Manifest.permission.ACCESS_COARSE_LOCATION)!=PackageManager.PERMISSION_GRANTED)
            {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this@MainActivity,
                        Manifest.permission.ACCESS_FINE_LOCATION) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(this@MainActivity,
                        Manifest.permission.ACCESS_COARSE_LOCATION)){

                   val builder=AlertDialog.Builder(this@MainActivity)
                    builder.setTitle("Ruxsat bering!!!")
                    builder.setMessage("manzilni olishga ruxsat bering")
                        .setPositiveButton("Xa"){ _, _ ->
                           ActivityCompat.requestPermissions(
                               this@MainActivity, arrayOf(
                                   Manifest.permission.ACCESS_FINE_LOCATION,
                                   Manifest.permission.ACCESS_COARSE_LOCATION),100
                           )
                        }
                        .setNegativeButton("Yo'q"){dialogInterface, i ->
                            Toast.makeText(applicationContext,"No did not",Toast.LENGTH_SHORT).show()
                        }.create().show()
                }else{
                    ActivityCompat.requestPermissions(
                        this@MainActivity, arrayOf(
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION),100
                    )
                }
            }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode==RESULT_OK&&requestCode==100)
            if (grantResults[0]+grantResults[1]==PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this,"Granted",Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this,"not granted",Toast.LENGTH_SHORT).show()
        }
        return
    }

    /*
    override fun onStart() {
        super.onStart()
        foodPreference.registerPref(this,this)
    }

    override fun onStop() {
        super.onStop()
        foodPreference.unregisterPref(this,this)

    }


    override fun onSharedPreferenceChanged(p0: SharedPreferences?, p1: String?) {
            menu.setItemSelected(R.id.homes)
            if(foodPreference.getExistFood(EXIST_FOOD)!!){
                val cc=foodPreference.getCounter(COUNTER)
                if (cc != null) {
                    menu.showBadge(R.id.boxes, cc)
                }
            }
    }
*/

    var doubleBackToExitPressedOnce = false
    override fun onBackPressed() {
        val count = supportFragmentManager.backStackEntryCount
        when {
            count == 1 -> {
                supportFragmentManager.popBackStack()
            }
            doubleBackToExitPressedOnce -> {
                finishAndRemoveTask()
                return
            }
            else -> {
                doubleBackToExitPressedOnce = true
                Log.d("MainActivity", "getBackStackEntryCount3")
                Toast.makeText(this, "Chiqish uchun yana bir marta bosing", Toast.LENGTH_SHORT).show()
                Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 1500)
            }
        }
    }

}
