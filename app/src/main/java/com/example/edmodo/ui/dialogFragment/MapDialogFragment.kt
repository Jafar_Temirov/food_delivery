package com.example.edmodo.ui.dialogFragment
import android.Manifest
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.example.edmodo.Preference.LocationPreference
import com.example.edmodo.R
import com.example.edmodo.util.LoadingDialog
import com.example.edmodo.database.FoodDatabase
import com.example.edmodo.repository.RestaurantsRepository
import com.example.edmodo.util.Constants
import com.example.edmodo.util.Function
import com.example.edmodo.util.Resource
import com.example.edmodo.viewModel.RestaurantDetailViewModel
import com.example.edmodo.viewModel.RestaurantDetailViewModelFactory
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.fragment_map_dialog.*
import java.io.IOException
import java.util.*

class MapDialogFragment : DialogFragment(),OnMapReadyCallback {

    lateinit var viewModel: RestaurantDetailViewModel
    private var first:Double = 0.0
    private  var second:Double = 0.0
    val TAG="MapDialogFragment"
    val btn: Button ?=null
    private lateinit var mMap: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    lateinit var loadingDialog: LoadingDialog
    lateinit var preference: LocationPreference
    lateinit var imageButton:ImageButton
    lateinit var btn_update_map:Button
    var text_view:TextView?=null
    var user_id:Int?=null

    override fun onMapReady(googleMap: GoogleMap?) {
        if (googleMap != null) {
            mMap = googleMap
            Function.showToast(requireActivity(), "Xarita ishlayapti !!!")
        }

         Log.e(TAG,"Is map enabled")
         getDeviceLocation()
         if (ActivityCompat.checkSelfPermission(
                 requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION
             ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                 requireActivity(), Manifest.permission.ACCESS_COARSE_LOCATION
             ) != PackageManager.PERMISSION_GRANTED
         ) {
             return
         }

         mMap.setPadding(1, 1, 1, 90)
         mMap.uiSettings.isMyLocationButtonEnabled = false;
         mMap.isMyLocationEnabled = true;
         mMap.uiSettings.isZoomControlsEnabled = true;

         init()
         mMap.setOnMapClickListener { latLng ->
             val markerOptions = MarkerOptions().position(latLng)
                 .title(latLng.latitude.toString() + ", " + latLng.longitude)
                 .snippet("Welcome to there !!!")
                 .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
             mMap.clear()
             mMap.addMarker(markerOptions);
             first = latLng.latitude
             second = latLng.longitude
             geoLocation(first, second)
         }

     }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullscreenDialogtheme)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view:View= inflater.inflate(R.layout.fragment_map_dialog, container, false)
        imageButton=view.findViewById(R.id.myown_location_dialog)
        btn_update_map=view.findViewById(R.id.btn_update_map)
        text_view=view.findViewById(R.id.manzil_map_dialog)
        loadingDialog= LoadingDialog(requireActivity())
        preference= LocationPreference(requireActivity())
        val newsRepository= RestaurantsRepository(FoodDatabase(requireActivity()))
        val viewmodelprovderFactory= RestaurantDetailViewModelFactory(
            requireActivity().application,
            newsRepository
        )
        viewModel= ViewModelProvider(this, viewmodelprovderFactory).get(RestaurantDetailViewModel::class.java)
        view.findViewById<ImageButton>(R.id.myown_exit).setOnClickListener { dismiss() }
        initMap()
        return view
    }

    private fun init(){
        myown_location_dialog.setOnClickListener {
            getDeviceLocation()
        }
        var ss=""
        btn_update_map.setOnClickListener {
            ss=text_view!!.text.toString()
            user_id= preference.getUserId(Constants.USER_ID)
            Log.d(TAG, "First $first, Second $second TEXT " + ss)
            viewModel.UpdateLocation(first.toString(), second.toString(), user_id!!, ss)
            viewModel.updateLocation.observe(
                viewLifecycleOwner,
                androidx.lifecycle.Observer { response ->
                    when (response) {
                        is Resource.Success -> {
                            hideProgressbar()
                            response.data?.let { user ->
                                Log.d(TAG, user.toString())
                                if (user.success && user.message.equals("inserted")) {
                                    preference.setLocationLat(Constants.LATITUDE, first)
                                    preference.setLocationLong(Constants.LONGTITUDE, second)
                                    preference.setManzil(Constants.MANZIL, ss)
                                }
                                dismiss()
                            }

                        }

                        is Resource.Error -> {
                            hideProgressbar()
                            response.message?.let {
                                Log.d(TAG, it.toString())
                            }
                        }
                        is Resource.Loading -> {
                            showProgressbar()
                        }
                    }
                })
        }


    }

    private fun geoLocation(lat: Double, long: Double){
        val geocoder = Geocoder(requireActivity(), Locale.getDefault())
        var list :List<Address>?=null
        try {
            list=geocoder.getFromLocation(lat, long, 2)
        }catch (e: IOException){
            Log.d(TAG, "geoLocate:  IOException: " + e.localizedMessage)
        }
        if (list!!.isNotEmpty()){
            val address: Address =list[0]
            val address2: Address =list[1]
            Log.d(TAG, "geoLocate:  Found A Location $address")
            Log.e(TAG, "Address1 $address")
            Log.e(TAG, "Address2 $address2")
            var adres=""
            var feature=""

            adres = if (address.locality==null) {
                if (address2.locality==null) "" else address2.locality
            }else address.locality
            feature = if (address.featureName==null||address.featureName.trim()=="Unnamed Road") {
                if (address2.featureName==null||address2.featureName.trim()=="Unnamed Road") "" else address2.featureName
            }else address.featureName

            Log.d(TAG, "Manzil: $adres,$feature")
            text_view?.text="$adres, $feature"
            moveCamera(LatLng(address.latitude, address.longitude), 15f, address.getAddressLine(0))
        }
    }

    private fun getDeviceLocation(){
        Log.d(TAG, "Get Device Location")
        fusedLocationClient= LocationServices.getFusedLocationProviderClient(requireActivity())
        try {
                val location=fusedLocationClient.lastLocation
                location.addOnCompleteListener {
                    if (it.isSuccessful&&it!=null){
                        Log.d(TAG, "OnComplete : Found Location")
                        val currentLocation: Location =it.result as Location
                        moveCamera(
                            LatLng(currentLocation.latitude, currentLocation.longitude),
                            15f,
                            "My Location"
                        )
                        first=currentLocation.latitude
                        second=currentLocation.longitude
                        geoLocation(first, second)
                    }else{
                        Log.d(TAG, "OnComplete : Location is null")
                        Function.showToast(requireActivity(), "OnComplete : Location is null")
                    }
            }

        }catch (e: SecurityException){
            Log.e(TAG, "Get Device Location SecurityException " + e.localizedMessage)
        }
    }

    private fun moveCamera(latLng: LatLng, zoom: Float, title: String){
        Log.d(
            TAG,
            "moveCamera : Moving the camera to Lat  " + latLng.latitude + " Long " + latLng.longitude
        )
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom))
        val options: MarkerOptions = MarkerOptions()
            .position(latLng)
            .title(title)
        mMap.addMarker(options)
        mMap.clear();
        mMap.addMarker(options);
        first=latLng.latitude;
        second=latLng.longitude;

        hideSoftKeyboard()
    }

    private fun initMap(){
        val mapFragment: SupportMapFragment =childFragmentManager.findFragmentById(R.id.map_dialog) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onActivityCreated(arg0: Bundle?) {
        super.onActivityCreated(arg0)
        dialog!!.window!!.attributes.windowAnimations = R.style.Animation_fast
    }

    private fun hideSoftKeyboard(){activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN) }

    private fun hideProgressbar(){
       // loadingDialog.dismissDialog()
    }
    private fun showProgressbar(){
       // loadingDialog.startLoading()
    }



}