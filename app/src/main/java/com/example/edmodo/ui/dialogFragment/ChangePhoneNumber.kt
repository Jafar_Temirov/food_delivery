package com.example.edmodo.ui.dialogFragment

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.RelativeLayout
import androidx.fragment.app.DialogFragment
import com.example.edmodo.R
import com.example.edmodo.util.Function
import com.github.pinball83.maskededittext.MaskedEditText

class ChangePhoneNumber : DialogFragment() {
    lateinit var btn_verify_dialog:Button
    lateinit var editText:MaskedEditText
    val TAG="ChangePhoneNumber"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setStyle(STYLE_NORMAL, R.style.FullscreenDialogtheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        val view:View=inflater.inflate(R.layout.fragment_change_phone_number, container, false)
        val relative=view.findViewById<RelativeLayout>(R.id.relativelayout_vertification)
        btn_verify_dialog=view.findViewById(R.id.btn_verify_dialog)
        editText=view.findViewById(R.id.vertification_number_dialog)
        btn_verify_dialog.setOnClickListener {
            val ss=editText.getUnmaskedText().toString()
            if (ss.isNotEmpty()){

                relative.visibility= View.VISIBLE
                Handler().postDelayed({
                    val dialog3:DialogFragment=VertificationCodeDialog()
                    val bundle = Bundle()
                    bundle.putString("number","+998"+editText.unmaskedText)
                    dialog3.arguments=bundle
                    dialog3.show(childFragmentManager,"tag3")
                    Log.d(TAG,"+998"+editText.unmaskedText)
                   // dismiss()
                    relative.visibility= View.GONE
                },1000)

            }else{
                Function.showToast(requireActivity(),"Telefon raqamizni kiriting !!!")
            }
        }

        return view
    }

    override fun onActivityCreated(arg0: Bundle?) {
        super.onActivityCreated(arg0)
        dialog!!.window!!.attributes.windowAnimations = R.style.DialogAnimation
    }

}