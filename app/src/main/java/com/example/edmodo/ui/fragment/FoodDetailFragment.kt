package com.example.edmodo.ui.fragment
import android.app.AlertDialog
import android.content.ContentValues
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.example.edmodo.R
import com.example.edmodo.database.FoodDatabase
import com.example.edmodo.database.entity.FoodEntity
import com.example.edmodo.repository.RestaurantsRepository
import com.example.edmodo.util.Constants
import com.example.edmodo.util.Constants.Companion.COUNTER
import com.example.edmodo.util.Constants.Companion.DISTANCE
import com.example.edmodo.util.Constants.Companion.DISTANCE_REAL
import com.example.edmodo.util.Constants.Companion.EXIST_FOOD
import com.example.edmodo.util.Constants.Companion.RESTAURANT_ID
import com.example.edmodo.util.Constants.Companion.RESTAURANT_ID_REAL
import com.example.edmodo.util.Constants.Companion.RESTAURANT_IMAGE_URI
import com.example.edmodo.util.Constants.Companion.RESTAURANT_IMAGE_URI_REAL
import com.example.edmodo.util.Constants.Companion.RESTAURANT_NAME
import com.example.edmodo.util.Constants.Companion.RESTAURANT_NAME_REAL
import com.example.edmodo.Preference.FoodPreference
import com.example.edmodo.util.Function
import com.example.edmodo.Preference.RestaurantPreference
import com.example.edmodo.viewModel.RestaurantsViewModel
import com.example.edmodo.viewModel.RestaurantsViewModelProviderFactory
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import java.lang.Exception
class FoodDetailFragment : DialogFragment() {

    lateinit var viewModel: RestaurantsViewModel
    lateinit var toolbar: Toolbar
    lateinit var preference: FoodPreference
    lateinit var restaurantPreference: RestaurantPreference
    lateinit var collapsingToolbar:CollapsingToolbarLayout
    lateinit var food_orders: FoodEntity
    lateinit var food_name:String
    lateinit var food_description:String
    lateinit var food_cost:String
    lateinit var food_image:String
    lateinit var menu_id:String
    var food_id:Int?=null
    var restaurant_id:Int?=null
    val TAG="FoodDetailFragment"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
      //  requireActivity().window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        setStyle(STYLE_NORMAL, R.style.FullscreenDialogtheme)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view:View= inflater.inflate(R.layout.fragment_food_detail, container, false)
        val repository= RestaurantsRepository(FoodDatabase(requireActivity()))

         val viewModelFactory= RestaurantsViewModelProviderFactory(requireActivity().application,repository)
         viewModel= ViewModelProvider(requireActivity(),viewModelFactory).get(RestaurantsViewModel::class.java)
         toolbar=view.findViewById(R.id.anim_toolbar_fooddetail)
         collapsingToolbar=view.findViewById(R.id.collapsing_toolbar_fooddetail)
         preference= FoodPreference(requireActivity())
         restaurantPreference= RestaurantPreference(requireActivity())
         if (activity is AppCompatActivity){
             (activity as AppCompatActivity).setSupportActionBar(toolbar)
            // (activity as AppCompatActivity).supportActionBar?.title = "Ovqatlar"
             (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
         }
         toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24)
         toolbar.setNavigationOnClickListener { dismiss()}
         collapsingToolbar.setContentScrimColor(Color.parseColor("#FF1A235F"))
        if(arguments!=null){
            food_name= requireArguments().getString("food_name").toString()
            food_description= requireArguments().getString("food_description").toString()
            food_cost= requireArguments().getString("food_cost").toString()
            food_image= requireArguments().getString("food_image").toString()
            menu_id= requireArguments().getString("food_menu_id").toString()
            food_id=requireArguments().getInt("food_id")
        }
        val textviewCost=view.findViewById<TextView>(R.id.food_cost_fooddetail)
        val textviewtitle=view.findViewById<TextView>(R.id.food_title_fooddetail)
        val textviewdesc=view.findViewById<TextView>(R.id.food_description_fooddetail)
        val imageview=view.findViewById<ImageView>(R.id.header_fooddetail)
        val progressBar=view.findViewById<ProgressBar>(R.id.progressbar_fooddetail)
        val fab=view.findViewById<FloatingActionButton>(R.id.fab_fooddetail)
        val textmoney=view.findViewById<TextView>(R.id.fooddetail_overallmoney)
        val fooddetail_add=view.findViewById<ImageView>(R.id.fooddetail_add)
        val fooddetail_minus=view.findViewById<ImageView>(R.id.fooddetail_minus)
        val fooddetail_count=view.findViewById<TextView>(R.id.fooddetail_count)
        val fooddetail_dostavka=view.findViewById<RelativeLayout>(R.id.fooddetail_dostavka)

        var counter=1
        var umumiy_counter=preference.getCounter(COUNTER)
        restaurant_id=preference.getRestaurantId(RESTAURANT_ID)
        var summ:Int?=null
        textviewCost.text=food_cost+" so'm"
        textviewtitle.text=food_name
        textviewdesc.text=food_description
        textmoney.text=food_cost+" so'm"
        if (activity is AppCompatActivity){ (activity as AppCompatActivity).supportActionBar?.title =food_name}
        fab.setOnClickListener {
            fab.setImageResource(R.drawable.likeon)
        }
        food_cost= food_cost.replace("\\s".toRegex(), "")
        summ= food_cost.toInt()
        fooddetail_add.setOnClickListener {
            counter++
            fooddetail_count.text=counter.toString()
            summ= counter*(food_cost.toInt())
            textmoney.text=Function.formatlash(summ!!).toString()+" so'm"
        }

        fooddetail_minus.setOnClickListener {
            if (counter>1){
                counter--
                fooddetail_count.text=counter.toString()
                summ= counter*(food_cost.toInt())
                textmoney.text=Function.formatlash(summ!!).toString()+" so'm"
            }
        }

        fooddetail_dostavka.setOnClickListener {
            if (preference.getExistFood(EXIST_FOOD)!!){
                Log.d(TAG,"Exist Food true")
                if (preference.getRestaurantId(RESTAURANT_ID)==restaurantPreference.getRestaurantRealId(RESTAURANT_ID_REAL)){

                    preference.getRestaurantId(RESTAURANT_ID)?.let { it1 -> restaurantPreference.setRestaurantRealId(RESTAURANT_ID_REAL, it1) }
                    preference.getRestaurant_ImageUri(RESTAURANT_IMAGE_URI)?.let { it1 -> restaurantPreference.setRestaurantRealImage(RESTAURANT_IMAGE_URI_REAL, it1) }
                    preference.getRestaurantName(RESTAURANT_NAME)?.let { it1 -> restaurantPreference.setRestaurantRealName(RESTAURANT_NAME, it1) }
                    preference.getDistance(DISTANCE)?.let { it1 -> restaurantPreference.setDistance(DISTANCE_REAL, it1) }

                        summ?.let { it1 -> preference.setWholeMoney(Constants.MONEY, it1) }
                        preference.setCounter(COUNTER,counter+umumiy_counter!!)
                        viewModel.setText("${counter+umumiy_counter}")
                        food_id?.let { it1 ->
                           viewModel.Comparing(it1)?.observe(viewLifecycleOwner, Observer {id->
                               try {
                                   val dd:Int=id
                                   viewModel.UpdateCount(counter+dd,food_id!!)
                               }catch (e:Exception){
                                   Log.d(TAG,"XATO")
                                   food_orders=
                                       FoodEntity(
                                           food_name,
                                           food_cost,
                                           food_image,
                                           counter,
                                           food_id!!,
                                           restaurant_id!!
                                       )
                                   viewModel.InsertFoods(food_orders)
                               }

                           })
                       }

                    Function.showToast(requireActivity(),"Buyurtma qabul qilindi")
                    dismiss()
                }else{
                    val addDialog= AlertDialog.Builder(context)
                    addDialog.setTitle("Kechirasiz !!!")
                    addDialog.setMessage("Siz boshqa restorandan taom qo'shgansiz. Bu restorandan buyurtma berish uchun mavjud buyurtmani bering yoki savatchani bo'shating")
                    addDialog.setPositiveButton("OK"){ dialogInterface, i -> }.create().show()
                }
            }else{
                Log.d(TAG,"Exist Food false")
                preference.setExistFood(EXIST_FOOD,true)
                preference.getRestaurantId(RESTAURANT_ID)?.let { it1 -> restaurantPreference.setRestaurantRealId(RESTAURANT_ID_REAL, it1) }
                preference.getRestaurant_ImageUri(RESTAURANT_IMAGE_URI)?.let { it1 -> restaurantPreference.setRestaurantRealImage(RESTAURANT_IMAGE_URI_REAL, it1) }
                preference.getRestaurantName(RESTAURANT_NAME)?.let { it1 -> restaurantPreference.setRestaurantRealName(RESTAURANT_NAME_REAL, it1) }
                preference.getDistance(DISTANCE)?.let { it1 -> restaurantPreference.setDistance(DISTANCE_REAL, it1) }

                summ?.let { it1 -> preference.setWholeMoney(Constants.MONEY, it1) }
                preference.setCounter(COUNTER,counter+umumiy_counter!!)
                viewModel.setText("${counter+umumiy_counter}")
                food_orders= FoodEntity(
                    food_name,
                    food_cost,
                    food_image,
                    counter,
                    food_id!!,
                    restaurant_id!!
                )
                viewModel.InsertFoods(food_orders)
                Function.showToast(requireActivity(),"Buyurtma qabul qilindi")
                dismiss()
            }
        }
        val requestOptions = RequestOptions()
            .placeholder(R.color.card2)
            .error(R.color.red_100)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
        Glide.with(requireActivity())
            .applyDefaultRequestOptions(requestOptions)
            .load(Uri.parse(food_image))
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(p0: GlideException?, p1: Any?, p2: Target<Drawable>?, p3: Boolean): Boolean {
                    progressBar.visibility = View.GONE
                    return false
                }
                override fun onResourceReady(p0: Drawable?, p1: Any?, p2: Target<Drawable>?, p3: DataSource?, p4: Boolean): Boolean {
                    Log.d(ContentValues.TAG, "OnResourceReady")
                    progressBar.visibility = View.GONE
                    return false
                }
            })
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(imageview)

        return view
    }
    override fun onActivityCreated(arg0: Bundle?) {
        super.onActivityCreated(arg0)
        dialog!!.window!!.attributes.windowAnimations = R.style.DialogAnimation}
}