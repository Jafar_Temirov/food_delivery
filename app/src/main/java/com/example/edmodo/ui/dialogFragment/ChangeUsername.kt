package com.example.edmodo.ui.dialogFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import androidx.fragment.app.DialogFragment
import com.example.edmodo.R


class ChangeUsername : DialogFragment() {
    lateinit var ozgartirish:Button
    lateinit var username_ozgar:EditText
    lateinit var surname_ozgar:EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setStyle(STYLE_NORMAL, R.style.FullscreenDialogtheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view:View= inflater.inflate(R.layout.fragment_change_username, container, false)
        ozgartirish= view.findViewById(R.id.ozgartish_ozgar)
        username_ozgar=view.findViewById(R.id.username_ozgar)
        surname_ozgar=view.findViewById(R.id.surname_ozgar)
        view.findViewById<ImageView>(R.id.back_change).setOnClickListener { dismiss() }

        ozgartirish.setOnClickListener {
            ozgartirish.setText("Saqlash")

        }


        return view
    }

    override fun onActivityCreated(arg0: Bundle?) {
        super.onActivityCreated(arg0)
        dialog!!.window!!.attributes.windowAnimations = R.style.DialogAnimation
    }
}