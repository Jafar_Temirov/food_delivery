package com.example.edmodo.ui.activity
import android.Manifest
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.example.edmodo.Preference.LocationPreference
import com.example.edmodo.Preference.SplashScreenPreference
import com.example.edmodo.R
import com.example.edmodo.database.FoodDatabase
import com.example.edmodo.repository.RestaurantsRepository
import com.example.edmodo.ui.MainActivity
import com.example.edmodo.util.Constants
import com.example.edmodo.util.Constants.Companion.MANZIL
import com.example.edmodo.util.Function
import com.example.edmodo.util.LoadingDialog
import com.example.edmodo.util.Resource
import com.example.edmodo.viewModel.RestaurantDetailViewModel
import com.example.edmodo.viewModel.RestaurantDetailViewModelFactory
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.activity_map.*
import java.io.IOException
import java.util.*

class MapActivity : AppCompatActivity(), OnMapReadyCallback{

    lateinit var viewModel: RestaurantDetailViewModel
    lateinit var loadingDialog: LoadingDialog
    lateinit var preference: LocationPreference
    private var first:Double = 0.0
    private  var second:Double = 0.0
    lateinit var bottomsheet_location:TextView
    var sheetBehavior: BottomSheetBehavior<*>? = null
    var linearLayout:LinearLayout?=null
    val TAG="MapActivity"
    private lateinit var mMap: GoogleMap
    private val LOCATION_PERMISSION_REQUEST_CODE = 1234
    private val FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION
    private val COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    lateinit var splashScreenPreference: SplashScreenPreference
    //vars
    lateinit var btn_address_bottomsheet: Button
    lateinit var username_sheet:EditText
    lateinit var surname_sheet:EditText
    var searchtext:EditText?=null
    var mLocationPermission:Boolean=false

    override fun onMapReady(googleMap: GoogleMap?) {
        if (googleMap != null) {
            mMap = googleMap
            Function.showToast(this, "Xarita ishlayapti !!!")
        }
        if (mLocationPermission){
              if (ActivityCompat.checkSelfPermission(
                      this, Manifest.permission.ACCESS_FINE_LOCATION
                  ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                      this, Manifest.permission.ACCESS_COARSE_LOCATION
                  ) != PackageManager.PERMISSION_GRANTED) {
                  return
              }
              getDeviceLocation()
              mMap.setPadding(1, 1, 1, 95)
              mMap.uiSettings.isMyLocationButtonEnabled = true;
              mMap.isMyLocationEnabled = true;
              mMap.uiSettings.isZoomControlsEnabled = true;

              init()
              mMap.setOnMapClickListener { latLng->
                  val markerOptions = MarkerOptions().position(latLng)
                      .title(latLng.latitude.toString() + ",  " + latLng.longitude)
                      .snippet("Welcome to there !!!")
                      .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))

                  mMap.clear()
                  mMap.addMarker(markerOptions);
                  first=latLng.latitude
                  second=latLng.longitude
                  geoLocation(first, second)
              }

          }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_UNSPECIFIED);
            window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
        setContentView(R.layout.activity_map)

        preference= LocationPreference(this)
        splashScreenPreference= SplashScreenPreference(this)
        val newsRepository= RestaurantsRepository(FoodDatabase(this))
        val viewmodelprovderFactory= RestaurantDetailViewModelFactory(application, newsRepository)
        viewModel=ViewModelProvider(this, viewmodelprovderFactory).get(RestaurantDetailViewModel::class.java)
        loadingDialog= LoadingDialog(this)
        searchtext=findViewById(R.id.search_edit_map)
        linearLayout=findViewById(R.id.linearlayout_bottomsheet)
        bottomsheet_location=findViewById(R.id.bottomsheet_location)
        btn_address_bottomsheet=findViewById(R.id.btn_address_bottomsheet)
        username_sheet=findViewById(R.id.bottom_sheet_username)
        surname_sheet=findViewById(R.id.bottomsheet_surname)
        sheetBehavior = BottomSheetBehavior.from(bottom_sheet_map)
        if (isServicesOk()){
            getLocationPermission()
            init()
        }
       // initMap()
    }
    private fun init(){
        sheetBehavior?.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(view: View, v: Float) {}
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                Log.d(TAG, " On StateChanged")
                if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                } else {
                }
            }
        })
        manzil_saqlash.setOnClickListener {
            if ((sheetBehavior as BottomSheetBehavior<*>).state==BottomSheetBehavior.STATE_EXPANDED){
                (sheetBehavior as BottomSheetBehavior<*>).state=BottomSheetBehavior.STATE_COLLAPSED
             }else if ((sheetBehavior as BottomSheetBehavior<*>).state==BottomSheetBehavior.STATE_COLLAPSED){
                (sheetBehavior as BottomSheetBehavior<*>).state=BottomSheetBehavior.STATE_EXPANDED
            }
        }
        myown_location.setOnClickListener {
            getDeviceLocation()
        }
        searchtext!!.setOnEditorActionListener { textView, actionId, keyEvent ->
           if (actionId == EditorInfo.IME_ACTION_SEARCH
               || actionId == EditorInfo.IME_ACTION_DONE
               || keyEvent.getAction() === KeyEvent.ACTION_DOWN
               || keyEvent.getAction() === KeyEvent.KEYCODE_ENTER){

               geoLocate()
               hideSoftKeyboard()
           }
           false
        }
        btn_address_bottomsheet.setOnClickListener {
            if (!username_sheet.text.isNullOrEmpty()||!surname_sheet.text.isNullOrEmpty()){
                viewModel.SaveUsers(
                    username_sheet.text.toString(),
                    surname_sheet.text.toString(),
                    bottomsheet_location.text.toString(),
                    intent.getStringExtra("tel_number").toString(),
                    first.toString(),
                    second.toString()
                )
                Log.e(TAG, "User " + intent.getStringExtra("tel_number").toString())
                Log.e(
                    TAG,
                    "All Data " + username_sheet.text.toString() + surname_sheet.text.toString() +
                            bottomsheet_location.text.toString() + first.toString() + second.toString()
                )
                viewModel.user.observe(this, androidx.lifecycle.Observer { response ->
                    when (response) {
                        is Resource.Success -> {
                            hideProgressbar()
                            response.data.let { responses ->
                                Log.e(TAG, responses.toString())
                                if (responses!!.success && responses.message == "inserted") {
                                    splashScreenPreference.setIsFirstTime(Constants.ISFIRST, false)
                                    if (responses.user_id != null) {
                                        preference.setUserId(
                                            Constants.USER_ID,
                                            responses.user_id
                                        )
                                    }
                                    preference.setLocationLat(Constants.LATITUDE, first)
                                    preference.setLocationLong(Constants.LONGTITUDE, second)
                                    preference.setManzil(
                                        MANZIL,
                                        bottomsheet_location.text.toString()
                                    )
                                    Log.d(
                                        TAG,
                                        "User Id " + responses.user_id + first.toString() + second.toString()
                                    )
                                    val intent = Intent(this, MainActivity::class.java)
                                    startActivity(intent)
                                    finish()
                                } else Function.showToast(this, "Xatolik,Malumot Saqlanmadi")
                            }
                        }

                        is Resource.Error -> {
                            hideProgressbar()
                            response.message?.let { message ->
                                Function.showToast(this, "Xatolik $message")
                            }
                        }

                        is Resource.Loading -> {
                            showProgressbar()
                        }
                    }

                })
               } else Function.showToast(this, "Ism va familiyani kiriting !!!")

        }
        hideSoftKeyboard()
    }
    private fun geoLocate(){
       Log.d(TAG, "geoLocate geoLocate")
       val searchString=searchtext?.text.toString()
       val geocoder:Geocoder= Geocoder(this)
       var list :List<Address>?=null
       try {
           list=geocoder.getFromLocationName(searchString, 2)
       }catch (e: IOException){
           Log.d(TAG, "geoLocate:  IOException: " + e.localizedMessage)
       }
       if (list!!.isNotEmpty()){
           val address:Address=list[0]
           geoLocation(address.latitude, address.longitude)
       }
    }
    private fun geoLocation(lat: Double, long: Double){
        val geocoder = Geocoder(this, Locale.getDefault())
        var list :List<Address>?=null
        try {
            list=geocoder.getFromLocation(lat, long, 2)

        }catch (e: IOException){
            Log.d(TAG, "geoLocate:  IOException: " + e.localizedMessage)
        }

        if (list!!.isNotEmpty()){
            val address:Address=list[0]
            val address2:Address=list[1]
            Log.d(TAG, "geoLocate:  Found A Location $address")
            Log.e(TAG, "Address1 $address")
            Log.e(TAG, "Address2 $address2")
            var adres=""
            var feature=""
            adres = if (address.locality==null) {
                if (address2.locality==null) "" else address2.locality
            }else address.locality
            feature = if (address.featureName==null||address.featureName.trim()=="Unnamed Road") {
                if (address2.featureName==null||address2.featureName.trim()=="Unnamed Road") "" else address2.featureName
            }else address.featureName
            bottomsheet_location.text="Manzil: $adres,$feature"
            moveCamera(LatLng(address.latitude, address.longitude), 15f, address.getAddressLine(0))
        }
    }
    private fun getDeviceLocation(){
       Log.d(TAG, "Get Device Location")
       fusedLocationClient=LocationServices.getFusedLocationProviderClient(this)
       try {
           if (mLocationPermission){
                fusedLocationClient.lastLocation
                .addOnSuccessListener { location: Location? ->
                if (location!=null){
                        moveCamera(
                            LatLng(location.latitude, location.longitude),
                            15f,
                            "My Location"
                        )
                        first=location.latitude
                        second=location.longitude
                        geoLocation(first, second)
                    }else{
                        Log.d(TAG, "OnComplete : Location is null")
                        Function.showToast(this, "OnComplete : Location is null")
                    }
                }
           }

       }catch (e: SecurityException){
           Log.e(TAG, "Get Device Location SecurityException " + e.localizedMessage)
       }
   }
    private fun moveCamera(latLng: LatLng, zoom: Float, title: String){
       Log.d(
           TAG,
           "moveCamera : Moving the camera to Lat  " + latLng.latitude + " Long " + latLng.longitude
       )
       mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom))
           val options:MarkerOptions=MarkerOptions()
               .position(latLng)
               .title(title)
        mMap.addMarker(options)
        mMap.clear();
        mMap.addMarker(options);
        first=latLng.latitude;
        second=latLng.longitude;
        hideSoftKeyboard()
    }
    private fun initMap(){
        val mapFragment:SupportMapFragment=supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun getLocationPermission(){
        Log.d(TAG, "getLocationPermission: getting location permissions")
        val permissions = arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
        if (ContextCompat.checkSelfPermission(this.applicationContext, FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this.applicationContext, COURSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mLocationPermission = true
                initMap()
            }else{
                ActivityCompat.requestPermissions(
                    this,
                    permissions,
                    LOCATION_PERMISSION_REQUEST_CODE
                )
            }
        } else {
            ActivityCompat.requestPermissions(this, permissions, LOCATION_PERMISSION_REQUEST_CODE)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ){
        mLocationPermission=false
        when(requestCode){
            LOCATION_PERMISSION_REQUEST_CODE -> {
                if (grantResults.size > 0) {
                    for (i in grantResults.indices) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            mLocationPermission = false
                            break
                        }
                    }
                    mLocationPermission = true
                    initMap()
                    // initialize map
                }
            }
        }
    }


    private fun hideSoftKeyboard(){this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN) }
    private fun hideProgressbar(){
      loadingDialog.dismissDialog()
    }
    private fun showProgressbar(){
      loadingDialog.startLoading()
    }

    private fun isServicesOk() : Boolean {
        Log.d(TAG, "isServicesOk true")
        val available= GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this)
        when {
            available== ConnectionResult.SUCCESS -> {
                Log.d(TAG, "isServicesOk true Google Play Servies")
                return true
            }
            GoogleApiAvailability.getInstance().isUserResolvableError(available) -> {
                Log.d(TAG, "isServicesOk true Error")
                val dialog: Dialog = GoogleApiAvailability.getInstance().getErrorDialog(
                    this,
                    available,
                    9001
                )
                dialog.show()
            }
            else -> {
                Function.showToast(this, "No can't make map request")
            }
        }
        return false
    }


}