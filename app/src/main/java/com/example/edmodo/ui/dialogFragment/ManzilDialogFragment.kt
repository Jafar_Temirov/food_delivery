package com.example.edmodo.ui.dialogFragment

import android.app.Activity
import android.app.TimePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.edmodo.Preference.LocationPreference
import com.example.edmodo.Preference.RestaurantPreference
import com.example.edmodo.R
import com.example.edmodo.util.LoadingDialog
import com.example.edmodo.database.FoodDatabase
import com.example.edmodo.model.SavedFoods
import com.example.edmodo.repository.RestaurantsRepository
import com.example.edmodo.ui.fragment.HomeFragment
import com.example.edmodo.util.Constants
import com.example.edmodo.util.Constants.Companion.MANZIL
import com.example.edmodo.util.Function
import com.example.edmodo.util.Resource
import com.example.edmodo.viewModel.RestaurantDetailViewModel
import com.example.edmodo.viewModel.RestaurantDetailViewModelFactory
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.fragment_manzil_dialog.*
import lib.kingja.switchbutton.SwitchMultiButton
import java.util.*

class ManzilDialogFragment : DialogFragment() {

    lateinit var viewmodel: RestaurantDetailViewModel
    lateinit var switchMultiButton:SwitchMultiButton
    lateinit var radios:RadioGroup
    lateinit var preference:LocationPreference
    lateinit var restaurantPreference: RestaurantPreference
    lateinit var loadingDialog: LoadingDialog
    var server:SavedFoods?=null
    var umumiy_narx:String?=null
    var ovqat_narxi:String?=null
    var yetkazish_narxi:String?=null
    var ovqatlar=""
    var idOfPlace=0
    var textOfPlace="Квартира"
    var manzil=""

    val TAG="ManzilDialogFragment"
    lateinit var time_picker:Button
    var zakaz_btn:Button?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setStyle(STYLE_NORMAL, R.style.FullscreenDialogtheme)
        hideKeyboard(requireActivity())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view:View=inflater.inflate(R.layout.fragment_manzil_dialog, container, false)
        getDialog()?.getWindow()?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        val newsRepository= RestaurantsRepository(FoodDatabase(requireActivity()))
        val viewmodelprovderFactory= RestaurantDetailViewModelFactory(
            requireActivity().application,
            newsRepository
        )
        viewmodel= ViewModelProvider(requireActivity(), viewmodelprovderFactory).get(
            RestaurantDetailViewModel::class.java
        )

        loadingDialog= LoadingDialog(requireActivity())
        restaurantPreference=RestaurantPreference(requireActivity())
        preference= LocationPreference(requireActivity())
        zakaz_btn=view.findViewById(R.id.zakaz_btn)
        val qoshimcha_tel=view.findViewById<TextView>(R.id.qoshimcha_tel)
        val bundle: Bundle? =arguments
        umumiy_narx=bundle?.getString("umumiy_narx")
        ovqat_narxi=bundle?.getString("ovqat_narxi")
        yetkazish_narxi=bundle?.getString("yetkazish_narxi")
        ovqatlar=bundle?.getString("ovqatlar").toString()
        server=bundle!!.getSerializable("listofServer") as SavedFoods?
        Log.e(TAG, "TT " + server.toString())
        view.findViewById<TextView>(R.id.summ_up).text=bundle.getString(
            "umumiy_narx",
            "Malumot yo'q"
        )
        view.findViewById<Button>(R.id.manzil_btnd).text=preference.getManzil(Constants.MANZIL)
        switchMultiButton=view.findViewById(R.id.switchbutton)
        time_picker=view.findViewById(R.id.btn_radiobutton)
        radios=view.findViewById(R.id.radios)

        switchMultiButton.setText("Квартира", "Ҳовли", "Офис")
            .setOnSwitchListener { position, tabText ->
                Function.showToast(requireActivity(), "TabText:: $tabText Position:: $position")
                textOfPlace=tabText
                when(position){
                    0 -> {
                        kvartira_include.visibility = View.VISIBLE
                        dom_include.visibility = View.GONE
                        ofis_include.visibility = View.GONE
                        idOfPlace = 0
                    }
                    1 -> {
                        kvartira_include.visibility = View.GONE
                        dom_include.visibility = View.VISIBLE
                        ofis_include.visibility = View.GONE
                        idOfPlace = 1
                    }
                    2 -> {
                        kvartira_include.visibility = View.GONE
                        dom_include.visibility = View.GONE
                        ofis_include.visibility = View.VISIBLE
                        idOfPlace = 2
                    }
                }
            }

        view.findViewById<ImageView>(R.id.back_manzil_dialog).setOnClickListener {
            dismiss()
        }
        time_picker.setOnClickListener {
            val mcurrentTime: Calendar = Calendar.getInstance()
            val hour: Int = mcurrentTime.get(Calendar.HOUR_OF_DAY)
            val minute: Int = mcurrentTime.get(Calendar.MINUTE)
            val mTimePicker: TimePickerDialog
            mTimePicker = TimePickerDialog(
                requireActivity(),
                { timePicker, selectedHour, selectedMinute ->
                    time_picker.text = "$selectedHour:$selectedMinute"
                },
                hour,
                minute,
                true
            )

            mTimePicker.setTitle("Select Time")
            mTimePicker.show()
        }

        radios.setOnCheckedChangeListener { radioGroup, id ->
            if (id==R.id.current_time){
                time_picker.visibility=View.GONE
            }else if (id==R.id.specific_time){
                Function.showToast(requireActivity(), "Vaqtni belgilang.!!!")
                time_picker.visibility=View.VISIBLE
            }
        }

        zakaz_btn!!.setOnClickListener {
            hideKeyboard(requireActivity())
            val bottomSheet = BottomSheetDialog(requireActivity(), R.style.BottomsheetDemo)
            val view1 = LayoutInflater.from(requireActivity())
                .inflate(
                    R.layout.bottomsheet_confirm,
                    view.findViewById<View>(R.id.bottomSheetOrder) as LinearLayout?
                )
            view1.findViewById<View>(R.id.btn_dismisso).setOnClickListener {
                bottomSheet.dismiss()
            }
            view1.findViewById<View>(R.id.btn_closeo).setOnClickListener {
                bottomSheet.dismiss()
                manzil=preference.getManzil(MANZIL).toString()+"\n"+textOfPlace+"\n"
                when(idOfPlace){
                    0 -> {
                        manzil +="Dom: " + view.findViewById<TextView>(R.id.k_dom).text.toString() + "\n" +
                                "Kvartira: " + view.findViewById<TextView>(R.id.k_kvartira).text.toString() + "\n" +
                                "Etaj: " + view.findViewById<TextView>(R.id.k_etaj).text.toString() + "\n" +
                                "Podyez: " + view.findViewById<TextView>(R.id.k_podyez).text.toString() + "\n" +
                                "Orienter: " + view.findViewById<TextView>(R.id.k_orienter).text.toString() + "\n" +
                                "Izoh qoldiring: " + view.findViewById<TextView>(R.id.k_izoh).text.toString()
                    }
                    1 -> {
                        manzil +="Orienter: " + view.findViewById<TextView>(R.id.d_orienter).text.toString() + "\n" +
                                "Izoh qoldiring: " + view.findViewById<TextView>(R.id.d_izox).text.toString()
                    }
                    2 -> {
                        manzil += "Ofis Nomi: " + view.findViewById<TextView>(R.id.o_ofisname).text.toString() + "\n" +
                                "Orienter: " + view.findViewById<TextView>(R.id.o_orienter).text.toString() + "\n" +
                                "Izoh qoldiring: " + view.findViewById<TextView>(R.id.o_izoh).text.toString()
                    }
                }

                viewmodel.SaveOrders(
                    preference.getUserId(Constants.USER_ID)!!.toInt(),
                    restaurantPreference.getRestaurantRealName(Constants.RESTAURANT_NAME_REAL)
                        .toString(),
                    restaurantPreference.getRestaurantRealId(Constants.RESTAURANT_ID_REAL)!!.toInt(),
                    umumiy_narx.toString(),
                    ovqat_narxi.toString(),
                    yetkazish_narxi.toString(),
                    ovqatlar,
                    manzil, qoshimcha_tel.text.toString(), time_picker.text.toString()
                )

                viewmodel.orders.observe(viewLifecycleOwner, androidx.lifecycle.Observer { response ->
                    when (response) {
                        is Resource.Success -> {
                            hideProgressbar()
                            response.data?.let { data ->
                                Log.e(TAG, "Response:: $data")
                                if (data.success && data.message == "inserted") {
                                    Function.showToast(requireActivity(), "Malumot Saqlandi !!!")
                                    val preferences: SharedPreferences =
                                        requireActivity().getSharedPreferences(
                                            R.string.restaurant_file.toString(),
                                            0
                                        )
                                    preferences.edit().clear().apply()
                                    val preferences2: SharedPreferences =
                                        requireActivity().getSharedPreferences(
                                            R.string.second_file.toString(),
                                            0
                                        )
                                    preferences2.edit().clear().apply()
                                    viewmodel.deleteAllOreders()
                                    dismiss()
                                    val fragment: Fragment = HomeFragment()
                                    requireActivity().supportFragmentManager
                                        .beginTransaction()
                                        .replace(R.id.fragment_container, fragment)
                                        .commit()

                                } else Function.showToast(
                                    requireActivity(),
                                    "Xatolik Malumot Saqlanmadi !!!"
                                )
                            }
                        }
                        is Resource.Error -> {
                            hideProgressbar()
                            response.message?.let { error ->
                                Log.e(TAG, error)
                            }
                        }
                        is Resource.Loading -> {
                            showProgressbar()
                        }

                    }

                })

            }
            bottomSheet.setContentView(view1)
            bottomSheet.show()
        }

        return view
    }

    override fun onActivityCreated(arg0: Bundle?) {
        super.onActivityCreated(arg0)
        dialog!!.window!!.attributes.windowAnimations = R.style.DialogAnimation
    }
    private fun hideProgressbar(){
      loadingDialog.dismissDialog()
    }
    private fun showProgressbar(){
        loadingDialog.startLoading()
    }

    private fun hideKeyboard(activity: Activity) {
        val imm: InputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun onDismiss(dialog: DialogInterface) {
        val imm = qoshimcha_tel.getContext()
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (imm.isActive) imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS)
        super.onDismiss(dialog)
    }
}