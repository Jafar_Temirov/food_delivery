package com.example.edmodo.ui.fragment

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.edmodo.R
import com.example.edmodo.adapters.RestaurantsAdapter
import com.example.edmodo.model.Restaurants
import com.example.edmodo.ui.MainActivity
import com.example.edmodo.ui.dialogFragment.MapDialogFragment
import com.example.edmodo.ui.dialogFragment.ProfileFragment
import com.example.edmodo.ui.dialogFragment.SearchFragment
import com.example.edmodo.util.Constants
import com.example.edmodo.util.Function
import com.example.edmodo.Preference.LocationPreference
import com.example.edmodo.util.Constants.Companion.MANZIL
import com.example.edmodo.util.Resource
import com.example.edmodo.viewModel.RestaurantsViewModel
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*

class HomeFragment : Fragment(), RestaurantsAdapter.OnItemClickListener, SharedPreferences.OnSharedPreferenceChangeListener {
    val ERROR_DIALOG_REQUEST=9001
    lateinit var viewModel: RestaurantsViewModel
    lateinit var restaurantsAdapter: RestaurantsAdapter
    lateinit var recyclerView: RecyclerView
    private var layoutManager: RecyclerView.LayoutManager? = null
    lateinit var localpreference: LocationPreference
    lateinit var search:ImageView
    lateinit var profile:ImageView
    lateinit var mazil_title:TextView
    val TAG="HomeFragment"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view:View=inflater.inflate(R.layout.fragment_home,container,false)
        viewModel=(activity as MainActivity).viewModel
        localpreference= LocationPreference(requireActivity())

        recyclerView=view.findViewById(R.id.home_recyclerview)
        search=view.findViewById(R.id.search_home)
        search.setOnClickListener {view1->
            val dialog3:DialogFragment=SearchFragment()
            dialog3.show(childFragmentManager,"tag3")
        }
        profile=view.findViewById(R.id.profile_home)
        profile.setOnClickListener {
            val dialog3:DialogFragment=ProfileFragment()
            dialog3.show(childFragmentManager,"tag3")
        }
        view.relative_main_tagida.setOnClickListener {
            if (isServicesOk()) {
                if (isMapsEnabled()) {
                    val dialog3: DialogFragment = MapDialogFragment()
                    dialog3.show(childFragmentManager, "tag3")
                }
            }
        }
        mazil_title=view.findViewById(R.id.mazil_title)
        mazil_title.text=localpreference.getManzil(Constants.MANZIL)
        mazil_title.setOnClickListener {
            if (isServicesOk()) {
                if (isMapsEnabled()) {
                    val dialog3: DialogFragment = MapDialogFragment()
                    dialog3.show(childFragmentManager, "tag3")
                }
            }
        }
        layoutManager=LinearLayoutManager(activity)
        recyclerView.layoutManager = layoutManager
        recyclerView.apply {
            itemAnimator=DefaultItemAnimator()
            isNestedScrollingEnabled=false
            setHasFixedSize(true)
        }
        viewModel.getRestaurantsList()
        viewModel.restaurant.observe(viewLifecycleOwner, Observer {response->
            when(response){
                is Resource.Success->{
                    hideProgressbar()
                    response.data?.let { initRecyclerview(it)
                    }
                }

              is Resource.Error->{
                hideProgressbar()
                response.message?.let { message->
                    Log.d(TAG,"Error : $message")
                    Toast.makeText(activity,"An error occured: $message",Toast.LENGTH_LONG).show()
                }
             }
              is Resource.Loading->{
                  showProgressbar()
              }
            }
        })
        return view
    }

    private fun initRecyclerview(list: List<Restaurants>){
        restaurantsAdapter=RestaurantsAdapter(requireActivity(),list,this)
        recyclerView.adapter = restaurantsAdapter
        restaurantsAdapter.submitList(list)
    }

    private fun hideProgressbar(){
        shimmerlayout_home.stopShimmer()
        shimmerlayout_home.visibility=GONE
    }
    private fun showProgressbar(){
        shimmerlayout_home.startShimmer()
        shimmerlayout_home.visibility= VISIBLE
    }

    override fun onItemClick(item: Restaurants, position: Int) {
        val dialog2: DialogFragment =RestaurantDetailFragment()
        val bundle = Bundle()
        bundle.putString("restaurant_name",item.name)
        bundle.putString("restaurant_more_info",item.more_info)
        bundle.putString("restaurant_main_image",item.main_image)
        bundle.putString("restaurant_rating",item.rating)
        bundle.putString("restaurant_longtitude",item.longtitude)
        bundle.putString("restaurant_latitude",item.latitude)
        item.id?.let { bundle.putInt("restaurant_id", it) }
        dialog2.arguments=bundle
        dialog2.show(requireActivity().supportFragmentManager, "tag")
    }
    private fun isServicesOk() : Boolean {
        Log.d(TAG,"isServicesOk true")
        val available= GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(requireActivity())
        when {
            available== ConnectionResult.SUCCESS -> {
                Log.d(TAG,"isServicesOk true Google Play Servies")
                return true
            }
            GoogleApiAvailability.getInstance().isUserResolvableError(available) -> {
                Log.d(TAG,"isServicesOk true Error")
                val dialog: Dialog = GoogleApiAvailability.getInstance().getErrorDialog(requireActivity(),available,ERROR_DIALOG_REQUEST)
                dialog.show()
            }
            else -> {
                Function.showToast(requireActivity(),"No can't make map request")
            }
        }
        return false
    }

    private fun isMapsEnabled(): Boolean {
        val manager: LocationManager = requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps()
            return false
        }
        return true
    }
    private fun buildAlertMessageNoGps() {
        val builder= AlertDialog.Builder(requireActivity())
        builder.setMessage(getString(R.string.gps_info))
            .setCancelable(false)
            .setPositiveButton("Xa"){ dialogInterface, i ->
                val enableGpsIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivityForResult(enableGpsIntent, Constants.PERMISSIONS_REQUEST_ENABLE_GPS)
            }
            .setNegativeButton("Yo'q"){dialogInterface, i ->
                Toast.makeText(context,getString(R.string.tuen_on_gps), Toast.LENGTH_SHORT).show()
            }.create().show()
    }

    override fun onSharedPreferenceChanged(p0: SharedPreferences?, p1: String?) {
       if (p1.equals(MANZIL)){
           mazil_title.text=localpreference.getManzil(MANZIL)
       }
    }

    override fun onResume() {
        super.onResume()
        localpreference.registerLocPref(requireActivity(),this)
    }

    override fun onPause() {
        localpreference.unregisterLocPref(requireActivity(),this)
        super.onPause()
    }
}