package com.example.edmodo.ui.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.edmodo.R
import com.example.edmodo.adapters.MyAdapter
import com.example.edmodo.database.FoodDatabase
import com.example.edmodo.model.Food
import com.example.edmodo.repository.RestaurantsRepository
import com.example.edmodo.util.Resource
import com.example.edmodo.viewModel.RestaurantDetailViewModel
import com.example.edmodo.viewModel.RestaurantDetailViewModelFactory
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class ViewPagerFragment : Fragment(),MyAdapter.OnItemClickListener,SwipeRefreshLayout.OnRefreshListener {
    lateinit var viewModel: RestaurantDetailViewModel
    val TAG="ViewPagerFragment"
    private var param1: String? = null
    private var param2: String? = null
    lateinit var food: List<Food>
    lateinit var mRecyclerView: RecyclerView
    lateinit var mLayoutManager: LinearLayoutManager
    lateinit var mAdapter: MyAdapter
    private var food_id:Int?=null
    lateinit var swipeRefreshLayout:SwipeRefreshLayout
     var textView: TextView?=null
    //counting the number of swipes
    companion object {
       // @JvmStatic
        fun getInstance(param1: String): ViewPagerFragment {
           val fragment: ViewPagerFragment=ViewPagerFragment()
           val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            // args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
        }
    }

    override fun onResume() {
        super.onResume()
        val repository= RestaurantsRepository(FoodDatabase(requireActivity()))
        val viewModelFactory=RestaurantDetailViewModelFactory(requireActivity().application,repository)
        viewModel= ViewModelProvider(requireActivity(),viewModelFactory).get(RestaurantDetailViewModel::class.java)
        param1?.let {initView(it)}
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view:View= inflater.inflate(R.layout.fragment_view_pager, container, false)
        mRecyclerView = view.findViewById(R.id.recyclerview_viewpager)
        mLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        swipeRefreshLayout=view.findViewById(R.id.swipe_refresh_viewpager)
        textView=view.findViewById(R.id.recyni_nol_data)
        swipeRefreshLayout.setOnRefreshListener(this)
        swipeRefreshLayout.resources.getColor(R.color.title_text_color)
        mRecyclerView.layoutManager=mLayoutManager
        mRecyclerView.apply {
            itemAnimator= DefaultItemAnimator()
            isNestedScrollingEnabled=true
            setHasFixedSize(true)
        }

        return view
    }

    private fun initView(ss:String){
        food_id=ss.toInt()
        Log.e(TAG, "MENU ID:::$food_id")
        viewModel.getFoods(food_id!!)
        viewModel.foods.observe(viewLifecycleOwner,Observer{ response->
          when(response){
              is Resource.Success->{
                  hideProgressbar()
                  Log.d(TAG,"Success")
                  response.data?.let { food=it
                  initRecyclerview(food)}
              }
              is Resource.Error->{
                  hideProgressbar()
                  Log.d(TAG,"Error")
                  response.message?.let {message->
                      Toast.makeText(activity,"An error occured: $message", Toast.LENGTH_LONG).show()
                  }
              }
              is Resource.Loading->{
                  showProgressbar()
                  Log.d(TAG,"Loading")
              }
          }
        })
    }

    private fun initRecyclerview(list:List<Food>){
        if (list.size<=0){
            textView!!.visibility=View.VISIBLE
            mAdapter= MyAdapter(list,requireActivity(),this)
            mAdapter.submitList(list)
            mRecyclerView.adapter=mAdapter
        }else{
            mAdapter= MyAdapter(list,requireActivity(),this)
            mAdapter.submitList(list)
            mRecyclerView.adapter=mAdapter
        }
    }

    override fun onItemClick(item: Food, position: Int) {
        Log.d(TAG,"Item ID"+item.id)
        val dialog:DialogFragment=FoodDetailFragment()
        val bundle=Bundle()
        bundle.putString("food_name",item.food_title)
        bundle.putString("food_description",item.food_description)
        bundle.putString("food_cost",item.cost)
        bundle.putString("food_image",item.food_image)
        bundle.putString("food_menu_id",item.restaurant_menu_id)
        item.id.let { bundle.putInt("food_id", it) }
        dialog.arguments=bundle
        dialog.show(requireActivity().supportFragmentManager, "tag")
    }

    override fun onRefresh() {
        lifecycleScope.launch {
            delay(1000L)
            mRecyclerView.adapter=mAdapter
            mAdapter.notifyDataSetChanged()
            swipeRefreshLayout.isRefreshing=false
        }
    }

    private fun hideProgressbar(){
        swipeRefreshLayout.isRefreshing=false
    }
    private fun showProgressbar(){
        swipeRefreshLayout.isRefreshing=true
    }

}
