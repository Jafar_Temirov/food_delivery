package com.example.edmodo.ui.fragment
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.edmodo.R
import com.example.edmodo.adapters.SavatchaAdapter
import com.example.edmodo.database.entity.Cost
import com.example.edmodo.ui.MainActivity
import com.example.edmodo.ui.dialogFragment.ManzilDialogFragment
import com.example.edmodo.util.Constants.Companion.COUNTER
import com.example.edmodo.util.Constants.Companion.DISTANCE_REAL
import com.example.edmodo.util.Constants.Companion.EXIST_FOOD
import com.example.edmodo.util.Constants.Companion.MONEY
import com.example.edmodo.util.Constants.Companion.RESTAURANT_ID
import com.example.edmodo.util.Constants.Companion.RESTAURANT_IMAGE_URI_REAL
import com.example.edmodo.util.Constants.Companion.RESTAURANT_NAME_REAL
import com.example.edmodo.Preference.FoodPreference
import com.example.edmodo.util.Function
import com.example.edmodo.Preference.RestaurantPreference
import com.example.edmodo.model.SavedFoods
import com.example.edmodo.viewModel.RestaurantsViewModel
import kotlinx.android.synthetic.main.fragment_savatcha.view.*

class SavatchaFragment : Fragment() {
    lateinit var viewModel: RestaurantsViewModel
    lateinit var recyclerView: RecyclerView
    lateinit var umumiyhisob:List<Cost>
    lateinit var restaurantPreference: RestaurantPreference
    lateinit var foodPreference: FoodPreference
    val TAG="SavatchaFragment"
    var string=""
    private var toServer:ArrayList<SavedFoods>?=null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view:View= inflater.inflate(R.layout.fragment_savatcha, container, false)
        viewModel=(activity as MainActivity).viewModel
        restaurantPreference= RestaurantPreference(requireActivity())
        foodPreference= FoodPreference(requireActivity())
        val savatcha_ordercost=view.findViewById<TextView>(R.id.savatcha_ordercost)
        val fragment_savat_km=view.findViewById<TextView>(R.id.fragment_savat_km)
        val fragment_savat_title=view.findViewById<TextView>(R.id.fragment_savat_title)
        val adapterSavat=SavatchaAdapter(requireActivity(), mutableListOf(),viewModel)
        val imagemain=view.findViewById<ImageView>(R.id.image_main_savatcha)
        val savatcha_yetkazib_berish=view.findViewById<TextView>(R.id.savatcha_yetkazib_berish)
        val savatcha_umumiynarx=view.findViewById<TextView>(R.id.savatcha_umumiynarx)

        recyclerView=view.findViewById(R.id.recyclerview_savatcha)
        if (foodPreference.getExistFood(EXIST_FOOD)!!){
            recyclerView.apply {
                layoutManager=LinearLayoutManager(requireActivity())
                itemAnimator= DefaultItemAnimator()
                isNestedScrollingEnabled=false
                setHasFixedSize(true)
                adapter=adapterSavat
            }
            viewModel.getOrders().observe(viewLifecycleOwner, Observer {
                adapterSavat.list=it
                adapterSavat.notifyDataSetChanged()
                if (it.isEmpty()){
                    Log.d(TAG,"Xammasi o'chiriladi")
                    foodPreference.setExistFood(EXIST_FOOD,false)
                    foodPreference.setCounter(COUNTER,0)
                    foodPreference.setWholeMoney(MONEY,0)
                    restaurantPreference.setDistance(DISTANCE_REAL,"null")
                    restaurantPreference.setRestaurantRealId(RESTAURANT_ID,0)
                    restaurantPreference.setRestaurantRealImage(RESTAURANT_IMAGE_URI_REAL,"null")
                    restaurantPreference.setRestaurantRealName(RESTAURANT_NAME_REAL,"null")
                }
            })

            val jj: String? =restaurantPreference.getDistance(DISTANCE_REAL)
            val dd: Double =jj!!.toDouble()*1000
            Log.d(TAG,"DD $dd")
            savatcha_yetkazib_berish.text=Function.Formatlash(dd.toInt())+" so'm"

            viewModel.getSummOfFood().observe(viewLifecycleOwner, Observer {
                umumiyhisob=it
                var ss:Double=0.0
                var count=0

                for (i in umumiyhisob){
                    ss+=i.food_cost.toInt()*i.counter
                    count+=i.counter
                    Log.d(TAG, "Cost $ss Count $count ++${i.food_name}")
                    string+=i.food_name+": $count dona,"+"\n"
                    toServer?.add(SavedFoods(i.counter,i.food_id,i.food_name))
                }

                foodPreference.setCounter(COUNTER,count)
                viewModel.setText(count.toString())
                savatcha_ordercost.text=Function.Formatlash(ss.toInt())+" so'm"
                savatcha_umumiynarx.text=Function.Formatlash((dd+ss).toInt())+" so'm"
            })

            fragment_savat_km.text=restaurantPreference.getDistance(DISTANCE_REAL)+" km"
            fragment_savat_title.text=restaurantPreference.getRestaurantRealName(RESTAURANT_NAME_REAL)

            val requestOptions = RequestOptions()
                .placeholder(R.drawable.lavash2)
                .error(R.drawable.lavash2)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
            Glide.with(requireActivity())
                .applyDefaultRequestOptions(requestOptions)
                .load(Uri.parse(restaurantPreference.getRestaurantRealImage(RESTAURANT_IMAGE_URI_REAL)))
                .into(imagemain)
        }else{
            Function.showToast(requireActivity(),"Zakaz yo'q hozircha")
            view.location_savat_framelayout.visibility=View.GONE
        }

        view.location_savatcha.setOnClickListener {
            Log.d(TAG,toServer.toString())
            Log.d(TAG, "String $string")
            val bundle:Bundle=Bundle()
            bundle.putSerializable("listofServer",toServer)
            bundle.putString("umumiy_narx",savatcha_umumiynarx.text.toString())
            bundle.putString("ovqat_narxi",savatcha_ordercost.text.toString())
            bundle.putString("yetkazish_narxi",savatcha_yetkazib_berish.text.toString())
            bundle.putString("ovqatlar",string)
            val dialog3: DialogFragment =ManzilDialogFragment()
            dialog3.arguments=bundle
            dialog3.show(childFragmentManager,"manzil")
        }

        return view
    }

}