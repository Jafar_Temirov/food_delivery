package com.example.edmodo.ui.activity

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.edmodo.R
import com.example.edmodo.util.Constants
import com.example.edmodo.util.Function
import com.github.pinball83.maskededittext.MaskedEditText

class NumberVertificationActivity : AppCompatActivity() {
    val TAG="NumberVertification"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.activity_number_vertification)
        val editText = findViewById<MaskedEditText>(R.id.vertification_number)
        val relative=findViewById<RelativeLayout>(R.id.relativelayout_vertification)
        val btnVerify=findViewById<Button>(R.id.btn_verify)

        btnVerify.setOnClickListener {
            if (isMapsEnabled()){
                btnVerify.isEnabled = false
                val ss=editText.getUnmaskedText().toString()
                closeKeyboard()

                if (ss.isNotEmpty()){
                    relative.visibility= View.VISIBLE
                    Handler().postDelayed({
                        val intent = Intent(this, VertificationCodeActivity::class.java)
                        intent.putExtra("number", "+998" + editText.unmaskedText)
                        Log.d(TAG, "+998" + editText.unmaskedText)
                        startActivity(intent)
                        finish()
                        relative.visibility = View.GONE
                    }, 1000)

                }else{
                    Function.showToast(this, "Telefon raqamizni kiriting !!!")
                }
            }

           }
    }

    private fun closeKeyboard(){
        val view: View? =this.currentFocus
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view!!.windowToken, 0)
    }

    fun isMapsEnabled(): Boolean {
        val manager: LocationManager =getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps()
            return false
        }
        return true
    }
    private fun buildAlertMessageNoGps() {
        val builder= AlertDialog.Builder(this)
        builder.setMessage("Manzilni o'zgartirish va to'liq manzilni olish uchun GPS ni yoqishiz kerak, siz uni yoqishga rozimisiz ?")
            .setCancelable(false)
            .setPositiveButton("Xa"){ dialogInterface, i ->
                val enableGpsIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivityForResult(enableGpsIntent, Constants.PERMISSIONS_REQUEST_ENABLE_GPS)
            }
            .setNegativeButton("Yo'q"){dialogInterface, i ->
                Toast.makeText(this,"Gps yoqilmadi !!!", Toast.LENGTH_SHORT).show()
            }.create().show()
    }
}