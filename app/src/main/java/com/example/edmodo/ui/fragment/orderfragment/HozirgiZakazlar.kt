package com.example.edmodo.ui.fragment.orderfragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.edmodo.Preference.LocationPreference
import com.example.edmodo.R
import com.example.edmodo.adapters.HozirgiZakazAdapter
import com.example.edmodo.database.FoodDatabase
import com.example.edmodo.model.Orders
import com.example.edmodo.repository.OrderRepository
import com.example.edmodo.repository.RestaurantsRepository
import com.example.edmodo.util.Resource
import com.example.edmodo.viewModel.OrderViewModel
import com.example.edmodo.viewModel.OrderViewModelFactory
import com.example.edmodo.viewModel.RestaurantsViewModel
import com.example.edmodo.viewModel.RestaurantsViewModelProviderFactory

class HozirgiZakazlar :Fragment() {
    lateinit var viewModel: OrderViewModel
    lateinit var preference: LocationPreference
    lateinit var adapterhozir:HozirgiZakazAdapter
    lateinit var recyclerView: RecyclerView
    private var layoutManager: RecyclerView.LayoutManager? = null
    lateinit var swipeRefreshLayout: SwipeRefreshLayout
    var progressbar:ProgressBar?=null
    var textview:TextView?=null
    var user_id:Int?=null
    val TAG="HozirgiZakazlar"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        val repository= OrderRepository()
        val viewmodelprovderFactory= OrderViewModelFactory(
            requireActivity().application, repository)
        viewModel= ViewModelProvider(requireActivity(), viewmodelprovderFactory).get(
            OrderViewModel::class.java)
        fetchData()

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_hozirgi_zakazlar, container, false)
        progressbar=view.findViewById(R.id.hozir_progressBar)
        recyclerView=view.findViewById(R.id.recyclerview_hozirzakaz)
        swipeRefreshLayout=view.findViewById(R.id.swiperefresh_hozir)
        swipeRefreshLayout.setColorScheme(android.R.color.holo_blue_light);
        textview=view.findViewById(R.id.hozirzakaz_title)
        layoutManager= LinearLayoutManager(activity)
        recyclerView.layoutManager = layoutManager
        recyclerView.apply {
            itemAnimator= DefaultItemAnimator()
            isNestedScrollingEnabled=false
            setHasFixedSize(true)
        }

        swipeRefreshLayout.setOnRefreshListener { fetchData() }

        return view
    }

    private fun fetchData(){
        preference= LocationPreference(requireActivity())
        user_id=preference.getUserId(com.example.edmodo.util.Constants.USER_ID)
        viewModel.getUnconfirmed(user_id!!,1)
        viewModel.orders.observe(viewLifecycleOwner, Observer {response->
            when(response){
                is Resource.Success->{
                    hideProgressbar()
                    response.data?.let {
                        Log.d(TAG, it.toString())
                        initRecyclerview(it)
                    }

                }
                is Resource.Error->{
                    hideProgressbar()
                    response.message?.let { message->
                        Log.d(TAG,"Error : $message")
                        Toast.makeText(activity,"An error occured: $message", Toast.LENGTH_LONG).show()
                    }
                }
                is Resource.Loading->{
                    showProgressbar()
                }
            }
        })
    }

    private fun initRecyclerview(list: List<Orders>){
        Log.d(TAG,"ListOf: ${list.size}")
        adapterhozir= HozirgiZakazAdapter(requireActivity(),list)
        recyclerView.adapter = adapterhozir
        adapterhozir.notifyDataSetChanged()
        if (list.size<=0){
            textview!!.visibility=View.VISIBLE
        }else{
            textview!!.visibility=View.GONE
        }
    }
    private fun hideProgressbar(){
      //  progressbar!!.visibility=View.GONE
        swipeRefreshLayout.isRefreshing=false
    }
    private fun showProgressbar(){
      //  progressbar!!.visibility=View.INVISIBLE
        swipeRefreshLayout.isRefreshing=true
    }

}