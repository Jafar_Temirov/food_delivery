package com.example.edmodo.ui.fragment.orderfragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import java.util.*

class ViewPagerAdapter(fm: FragmentManager,var stringList:ArrayList<String>
                                           ,var fragmentList:ArrayList<Fragment>)
                       : FragmentPagerAdapter(fm,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
      return fragmentList[position]
    }

    override fun getCount(): Int {return stringList.size}

    override fun getPageTitle(position: Int): CharSequence? {
        return stringList[position]
    }

    fun AddFragment(fragment: Fragment,string: String){
        fragmentList.add(fragment)
        stringList.add(string)
    }

    override fun getItemPosition(`object`: Any): Int {
        return super.getItemPosition(`object`)
    }

}