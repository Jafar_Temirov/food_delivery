package com.example.edmodo.ui.fragment.orderfragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.example.edmodo.R
import com.google.android.material.tabs.TabLayout
import java.util.ArrayList

class OrdersFragment : Fragment() {
    private lateinit var tabLayout: TabLayout
    private lateinit var viewPager: ViewPager
    private lateinit var viewPagerAdapter: ViewPagerAdapter
    private var titlestring = ArrayList<String>()
    private var fragmentstring = ArrayList<Fragment>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view:View=inflater.inflate(R.layout.fragment_orders, container, false)
        tabLayout=view.findViewById(R.id.tablayout_orders)
        viewPager=view.findViewById(R.id.viewpager_orders)

        viewPagerAdapter= ViewPagerAdapter(childFragmentManager,titlestring,fragmentstring)
        viewPagerAdapter.AddFragment(HozirgiZakazlar(),"Hozirgi")
        viewPagerAdapter.AddFragment(OldingiZakazlar(),"Oldingi")
        viewPager.adapter=viewPagerAdapter
        tabLayout.setupWithViewPager(viewPager)
        return view
    }
}