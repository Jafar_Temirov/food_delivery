package com.example.edmodo.database.dao
import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.edmodo.database.entity.Cost
import com.example.edmodo.database.entity.FoodEntity

@Dao
interface FoodDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(foodDao: FoodEntity): Long

    @Query("UPDATE food_table SET counter=:counters WHERE food_id=:foodId")
    suspend fun update(counters:Int,foodId: Int)

    @Query("SELECT counter FROM food_table WHERE food_id= :foodId LIMIT 1")
    fun compareFood(foodId:Int): LiveData<Int>?

    @Query("SELECT * FROM food_table WHERE is_deleted=0 ORDER BY id DESC ")
    fun getAllFoods(): LiveData<MutableList<FoodEntity>>
    // WHERE is_deleted=0

    @Query("SELECT food_cost,counter,food_id,food_name FROM food_table WHERE is_deleted=0 ")
    fun getSumm(): LiveData<List<Cost>>

    @Delete
    suspend fun deleteFood(foodDao: FoodEntity)

    @Query("DELETE FROM food_table")
    suspend fun deleteAll()

    // Update data for deleting data within 5 second
    @Update
    suspend fun updateDefault(foodDao: FoodEntity)

}