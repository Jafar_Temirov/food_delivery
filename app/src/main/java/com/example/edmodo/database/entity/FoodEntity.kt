package com.example.edmodo.database.entity
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "food_table")
data class FoodEntity(
  var food_name:String,
  var food_cost:String,
  var food_url:String,
  var counter:Int,
  var food_id:Int,
  var restaurant_id:Int,
  @ColumnInfo(name ="is_deleted",defaultValue ="0")
  var is_deleted:Int=0
){
  @PrimaryKey(autoGenerate = true)
  var id:Int?=null
}