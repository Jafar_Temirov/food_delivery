package com.example.edmodo.database.entity

data class Cost(
    val counter:Int,
    val food_cost :String,
    val food_name:String,
    val food_id:Int)