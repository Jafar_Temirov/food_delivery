package com.example.edmodo.adapters

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.example.edmodo.R
import com.example.edmodo.model.Food
import kotlinx.android.synthetic.main.itemfoods.view.*

class MyAdapter(var data:List<Food>, var context: Context,var onclick: OnItemClickListener):
    RecyclerView.Adapter<MyAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.itemfoods,parent,false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun submitList(listofFoods: List<Food>){
     val oldItems=data
     val diffResult:DiffUtil.DiffResult=DiffUtil.calculateDiff(
         FoodItemDiffCalback(oldItems,listofFoods))
        data=listofFoods
        diffResult.dispatchUpdatesTo(this)
    }
    class FoodItemDiffCalback(var oldItems:List<Food>, var newItems:List<Food>)
        :DiffUtil.Callback(){
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return (oldItems.get(oldItemPosition).id==newItems.get(oldItemPosition).id)
        }

        override fun getOldListSize(): Int {
            return oldItems.size
        }

        override fun getNewListSize(): Int {
          return newItems.size
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldItems.get(oldItemPosition).equals(newItems.get(newItemPosition))
        }

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
          holder.item1.text=data[position].food_title
          holder.item2.text=data[position].food_description
          holder.item3.text=data[position].cost
        val requestOptions = RequestOptions()
            .placeholder(R.color.lightwhite)
            .error(R.color.red_100)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
          holder.item4.apply {
              Glide.with(context)
                  .applyDefaultRequestOptions(requestOptions)
                  .load(Uri.parse(data[position].food_image))
                  .transition(DrawableTransitionOptions.withCrossFade())
                  .into(holder.item4)
          }
        holder.initialize(data[position],onclick)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val item1=itemView.itemfood_name
        val item2=itemView.itemfood_description
        val item3=itemView.itemfood_cost
        val item4=itemView.itemfood_image

        fun initialize(item:Food,action:OnItemClickListener){
            itemView.setOnClickListener {
                action.onItemClick(item,adapterPosition)
            }
        }
    }

    interface OnItemClickListener{
        fun onItemClick(item:Food,position: Int)
    }
}
