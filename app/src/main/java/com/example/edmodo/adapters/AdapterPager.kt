package com.example.edmodo.adapters
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.edmodo.ui.fragment.ViewPagerFragment

class AdapterPager(fragmentManager: FragmentManager,
                   var id:ArrayList<Int>
                   ,var titleList:ArrayList<String>
                   ,var fragmentList:ArrayList<Fragment>) : FragmentStatePagerAdapter(fragmentManager,
    BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {


    override fun getItem(position: Int): Fragment {
        return ViewPagerFragment.getInstance(""+id[position])
    }

    override fun getCount(): Int {
        Log.d("Adapter size ss",titleList.size.toString())
    return titleList.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return titleList[position]
    }

    override fun getItemPosition(`object`: Any): Int {
        return super.getItemPosition(`object`)
    }

}