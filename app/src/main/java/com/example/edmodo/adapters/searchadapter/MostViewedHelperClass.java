package com.example.edmodo.adapters.searchadapter;

public class MostViewedHelperClass {
    String text;
    int image;

    public MostViewedHelperClass(int image, String text) {
        this.image = image;
        this.text = text;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
