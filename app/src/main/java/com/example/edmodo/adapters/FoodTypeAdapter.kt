package com.example.edmodo.adapters

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.example.edmodo.R
import com.example.edmodo.model.FoodType
import kotlinx.android.synthetic.main.most_viewed_card_design.view.*

class FoodTypeAdapter(var context: Context,var list:List<FoodType>)
    : RecyclerView.Adapter<FoodTypeAdapter.FoodTypeViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int): FoodTypeAdapter.FoodTypeViewHolder {
       return FoodTypeViewHolder(LayoutInflater.from(context).inflate
           (R.layout.most_viewed_card_design,parent,false))
    }

    override fun onBindViewHolder(holder: FoodTypeAdapter.FoodTypeViewHolder, position: Int) {
      val foodType:FoodType=list[position]
      holder.mv_desc.text=foodType.food_description
      holder.mv_title.text=foodType.food_title
      holder.mv_rating.rating= foodType.food_start.toFloat()

        val requestOptions = RequestOptions()
            .placeholder(R.color.card1)
            .error(R.color.red_100)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()

        Glide.with(context)
            .applyDefaultRequestOptions(requestOptions)
            .load(Uri.parse(foodType.food_image))
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(holder.mv_image)
    }

    override fun getItemCount(): Int {
       return list.size
    }

    class FoodTypeViewHolder(itemview: View):RecyclerView.ViewHolder(itemview) {
        val mv_image=itemview.mv_image
        val mv_title=itemview.mv_title
        val mv_rating=itemview.mv_rating
        val mv_desc=itemview.mv_desc
    }
}