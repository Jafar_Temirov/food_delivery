package com.example.edmodo.adapters

import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.example.edmodo.R
import com.example.edmodo.database.entity.FoodEntity
import com.example.edmodo.util.Function
import com.example.edmodo.viewModel.RestaurantsViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.item_savatcha.view.*

    class SavatchaAdapter(var context:Context,
                          var list: MutableList<FoodEntity>,
                          var viewModel: RestaurantsViewModel): RecyclerView.Adapter<SavatchaAdapter.SavatchaViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SavatchaViewHolder {
        return SavatchaViewHolder(LayoutInflater.from(context).inflate(R.layout.item_savatcha,parent,false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: SavatchaViewHolder, position: Int) {
        var listofData: FoodEntity =list[position]
        holder.food_savatcha.text=listofData.food_name
        holder.cost_savatcha.text=listofData.food_cost+" so'm"
        holder.savatcha_count.text= listofData.counter.toString()
        holder.savatcha_minus.setOnClickListener {
            val currentitem= listofData
            if (currentitem.counter>0){
                currentitem.counter--
                viewModel.InsertFoods(currentitem)
            }
            }
        holder.savatcha_add.setOnClickListener {
            val currentitem=listofData
            currentitem.counter++
            viewModel.InsertFoods(currentitem)
        }

        holder.close_savatcha.setOnClickListener {
            var id=holder.adapterPosition
            val dataw= listofData
            dataw.is_deleted=1
            viewModel.updateDefault(dataw)

      val timer=object : CountDownTimer(5000, 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    Log.e("Jafar:::","seconds remaining: " + millisUntilFinished / 1000)
                    //here you can have your logic to set text to edittext
                }
                override fun onFinish() {
                    viewModel.deleteOrders(listofData)
                    Function.showToast(context,"Taom o'chirildi")
                    Log.e("Jafar:::","done")
                }
            }.start()


            val snackbar=Snackbar.make(it,"O'chirishga rozimisiz .?",Snackbar.LENGTH_INDEFINITE)
            val snackbarLayout = snackbar.view
            val textView = snackbarLayout.findViewById<View>(com.google.android.material.R.id.snackbar_text) as TextView
            textView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_undo, 0)
            textView.compoundDrawablePadding =context.resources.getDimensionPixelOffset(R.dimen.one_dp)

            val params: FrameLayout.LayoutParams=snackbarLayout.layoutParams as FrameLayout.LayoutParams
            params.setMargins(10, 10, 10, 10)
            snackbarLayout.layoutParams=params

            snackbar.setActionTextColor(Color.parseColor("#2ab8f5"))
            snackbar.setTextColor(Color.WHITE)
            snackbar.duration = 5000
            snackbar.setAction("Undo", View.OnClickListener {
                    dataw.is_deleted=0
                    viewModel.updateDefault(dataw)
                    timer.cancel()
                })
            snackbar.show()
        }

        val requestOptions = RequestOptions()
            .placeholder(R.color.lightwhite)
            .error(R.color.red_100)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
        holder.savatcha_image.apply {
            Glide.with(context)
            .applyDefaultRequestOptions(requestOptions)
            .load(Uri.parse(listofData.food_url))
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(savatcha_images)
        }
    }

    class SavatchaViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
       val savatcha_image= itemView.savatcha_images!!
       val food_savatcha= itemView.food_savatcha!!
       val cost_savatcha= itemView.cost_savatcha!!
       val close_savatcha= itemView.close_savatcha!!
       val savatcha_minus= itemView.savatcha_minus!!
       val savatcha_add= itemView.savatcha_add!!
       val savatcha_count= itemView.savatcha_count!!
       val cardview_savat_item=itemView.cardview_savat_item!!
    }
}