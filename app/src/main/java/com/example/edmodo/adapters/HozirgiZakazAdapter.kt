package com.example.edmodo.adapters
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.edmodo.R
import com.example.edmodo.model.Orders
import kotlinx.android.synthetic.main.item_hozirgizakaz.view.*

class HozirgiZakazAdapter(var context: Context,var list:List<Orders>)
    : RecyclerView.Adapter<HozirgiZakazAdapter.HozirgiViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            : HozirgiZakazAdapter.HozirgiViewHolder {
     return HozirgiViewHolder(LayoutInflater.from(context).inflate(R.layout.item_hozirgizakaz,parent,false))
    }

    override fun onBindViewHolder(holder: HozirgiZakazAdapter.HozirgiViewHolder, position: Int) {
       val listOfData:Orders=list[position]
       holder.res_name.text=listOfData.restaurant_name
       holder.manzil.text=listOfData.manzil
       holder.ovqatlar.text="Buyurtmalar:\n"+listOfData.ovqatlar
       holder.buyurtma_narxi.text=  "Ovqat narxi:       "+listOfData.ovqat_narxi
       holder.yetkazish_narxii.text="Yetkazish narxi:   "+listOfData.yetkazish_narxi
       holder.umumiy_narx.text=     "Umumiy narx:       "+listOfData.umumiy_narx
       holder.time_order.text=listOfData.yetkazish_vaqt
       holder.zakaz_index.text="#${1000+listOfData.restaurant_id}"
    }

    override fun getItemCount(): Int {
      return list.size
    }

    class HozirgiViewHolder(itemview: View): RecyclerView.ViewHolder(itemview) {
      val res_name=itemview.hozir_res_name
      val manzil=itemview.hozir_manzil
      val ovqatlar=itemview.hozir_ovqat
      val buyurtma_narxi=itemview.hozir_buyurtma_narxi
      val yetkazish_narxii=itemview.hozir_yetkazib_berish
      val umumiy_narx=itemview.hozir_umumiy_narx
      val time_order=itemview.hozir_time_ordered
      val zakaz_index=itemview.hozir_zakaz_id
    }

}