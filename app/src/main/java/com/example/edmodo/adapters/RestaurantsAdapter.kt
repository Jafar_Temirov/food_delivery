package com.example.edmodo.adapters
import android.content.ContentValues.TAG
import android.content.Context
import android.graphics.drawable.Drawable
import android.location.Location
import android.net.Uri
import android.util.Half.toFloat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.example.edmodo.R
import com.example.edmodo.model.Restaurants
import com.example.edmodo.util.Constants
import com.example.edmodo.util.ItemAnimation
import com.example.edmodo.Preference.LocationPreference
import kotlinx.android.synthetic.main.item_article_preview.view.*
import java.text.DecimalFormat

class RestaurantsAdapter(var context: Context,var list: List<Restaurants>,var clicklidtrnrt:OnItemClickListener):
    RecyclerView.Adapter<RestaurantsAdapter.RestaurantViewHolder>() {
    lateinit var prefernce: LocationPreference

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestaurantsAdapter.RestaurantViewHolder {
       return RestaurantViewHolder(LayoutInflater.from(context)
           .inflate(R.layout.item_article_preview
               ,parent,false))
    }

    override fun getItemCount(): Int {
      return list.size
    }

    fun submitList(listRestaurant: List<Restaurants>){
        val oldItems=list
        val diffResult:DiffUtil.DiffResult=DiffUtil.calculateDiff(
            RestaurantItemDiffCalback(oldItems,listRestaurant))

        list=listRestaurant
       diffResult.dispatchUpdatesTo(this)
    }

    class RestaurantItemDiffCalback(
        var oldItems :List<Restaurants>,
        var newItems:List<Restaurants>
    ):DiffUtil.Callback(){
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
         return (oldItems.get(oldItemPosition).id==newItems.get(oldItemPosition).id)
        }

        override fun getOldListSize(): Int {
            return oldItems.size
        }

        override fun getNewListSize(): Int {
          return newItems.size
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldItems.get(oldItemPosition).equals(newItems.get(newItemPosition))
        }
    }

    class RestaurantViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        val name = itemView.food_title
        val more_info = itemView.food_description
        val image_main=itemView.food_image
        val progressbar=itemView.home_progressbar_photo
        val food_rating=itemView.restaurant_detail_rating
        val food_arriving=itemView.restdetail_arriving_time
        val food_price=itemView.restdetailfood_price

        fun initialize(item: Restaurants,action: OnItemClickListener){
            name.text=item.name
            more_info.text=item.more_info
            food_rating.text=item.rating+"(0)"
            itemView.setOnClickListener {
                action.onItemClick(item,adapterPosition)
            }
        }
    }

    private var onItemClickListener: ((Restaurants) -> Unit)? = null

    override fun onBindViewHolder(holder: RestaurantViewHolder, position: Int) {
        val post=list[position]
        val longtitude=post.longtitude
        val latitude=post.latitude
        prefernce= LocationPreference(context)
        if (prefernce!=null&&prefernce.getLocationLat(Constants.LATITUDE)!=null&&prefernce.getLocationLong(Constants.LONGTITUDE)!=null){
            val locationA = Location("point A")
            locationA.latitude = latitude.toFloat().toDouble()
            locationA.longitude =longtitude.toFloat().toDouble()
            val locationB = Location("point B")
            locationB.latitude = prefernce.getLocationLat(Constants.LATITUDE)!!.toDouble()
            locationB.longitude = prefernce.getLocationLong(Constants.LONGTITUDE)!!.toDouble()
            var distance: Float = locationA.distanceTo(locationB)
            Log.d("TAG", "Adapter Jafar $distance")
            var cost_arriving:Double?=null
            var dat:String?=null
            var nomer:Int?=null
            distance /= 1000
            cost_arriving=Math.round(distance * 10.0) / 10.0
            dat=cost_arriving.toString()+"km"
            cost_arriving *= 1500
            nomer= cost_arriving.toInt()
            val formatter = DecimalFormat("#,###,###")
            val ss: Number? =formatter.parse(nomer.toString())
            holder.food_arriving.text=dat
            holder.food_price.text= ss.toString()+" so'm"
            holder.name.text = post.name
            holder.more_info.text = post.more_info
            holder.food_rating.text=post.rating
            val requestOptions = RequestOptions()
                .placeholder(R.color.card1)
                .error(R.color.red_100)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()

            Glide.with(context)
                .applyDefaultRequestOptions(requestOptions)
                .load(Uri.parse(post.main_image))
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(p0: GlideException?, p1: Any?, p2: Target<Drawable>?, p3: Boolean): Boolean {
                        holder.progressbar.visibility = View.GONE
                        return false
                    }
                    override fun onResourceReady(p0: Drawable?, p1: Any?, p2: Target<Drawable>?, p3: DataSource?, p4: Boolean): Boolean {
                        Log.d(TAG, "OnResourceReady")
                        holder.progressbar.visibility = View.GONE
                        return false
                    }
                })
               //.transition(DrawableTransitionOptions.withCrossFade())
                .into(holder.image_main)

            setOnItemClickListener {
                onItemClickListener?.let { it(post)}
            }
            holder.initialize(list[position],clicklidtrnrt)
            ItemAnimation.animateFadeIn(holder.itemView,position)
        }
    }

    private fun setOnItemClickListener(listener: (Restaurants)->Unit){
        onItemClickListener=listener
    }

    interface OnItemClickListener{
        fun onItemClick(item:Restaurants,position: Int)
    }
}
