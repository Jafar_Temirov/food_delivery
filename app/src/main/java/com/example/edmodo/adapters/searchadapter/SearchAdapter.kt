package com.example.edmodo.adapters.searchadapter

import android.content.ContentValues
import android.content.Context
import android.graphics.drawable.Drawable
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.example.edmodo.R
import com.example.edmodo.model.Food
import kotlinx.android.synthetic.main.item_search.view.*

class SearchAdapter(var context: Context,var list: List<Food>)
    :RecyclerView.Adapter<SearchAdapter.SearchViewHolder>(){


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SearchViewHolder {
        return SearchViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_search, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        val food:Food=list[position]
        holder.search_description.text=food.food_description
        holder.search_food_title.text=food.food_title
        holder.search_narxi.text=food.cost+" so'm"
        val requestOptions = RequestOptions()
            .placeholder(R.color.card1)
            .error(R.color.red_100)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()

        Glide.with(context)
            .applyDefaultRequestOptions(requestOptions)
            .load(Uri.parse(food.food_image))
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(p0: GlideException?, p1: Any?, p2: Target<Drawable>?, p3: Boolean): Boolean {
                    holder.search_progress_item.visibility = View.GONE
                    return false
                }
                override fun onResourceReady(p0: Drawable?, p1: Any?, p2: Target<Drawable>?, p3: DataSource?, p4: Boolean): Boolean {
                    Log.d(ContentValues.TAG, "OnResourceReady")
                    holder.search_progress_item.visibility = View.GONE
                    return false
                }
            })
            .into(holder.search_food_image)
    }
    override fun getItemCount(): Int {
        return list.size
    }
    class SearchViewHolder(itemView:View):RecyclerView.ViewHolder(itemView) {
        val search_food_image=itemView.search_food_image
        val search_restaurant_image=itemView.search_restaurant_image
        val search_progress_item=itemView.search_progress_item
        val search_food_title=itemView.search_food_title
        val search_description=itemView.search_description
        val search_narxi=itemView.search_narxi
    }

}