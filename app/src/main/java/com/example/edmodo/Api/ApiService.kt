package com.example.edmodo.Api

import com.example.edmodo.model.*
import retrofit2.Response
import retrofit2.http.*

interface ApiService {
    // Auth
    @FormUrlEncoded
    @POST("food_order/saveusers.php")
    suspend fun saveUser(
        @Field("username") user:String,
        @Field("surname") surname:String,
        @Field("location") location:String,
        @Field("tel_number") tel_number:String,
        @Field("latitude") latitude:String,
        @Field("longtitude") longtitude:String
    ):Response<User>

    // Location
    @FormUrlEncoded
    @POST("food_order/updateLocation.php")
    suspend fun updateLocation(
        @Field("longtitude") longtitude:String,
        @Field("latitude") latitude:String,
        @Field("user_id") user_id:Int,
        @Field("manzil") manzil:String
    ):Response<User>


     @GET("food_order/getRestaurants.php")
     suspend fun getRestaurants()
             :Response<List<Restaurants>>

     @GET("food_order/getMenu.php")
     suspend fun getMenus(@Query("menu_id") id:Int)
            :Response<List<RestaurantMenu>>

     @GET("food_order/getFood.php")
     suspend fun getFoods(@Query("id") id:Int)
            :Response<List<Food>>

     @FormUrlEncoded
     @POST("food_order/SaveOrders.php")
     suspend fun SaveOrder(
       @Field("user_id")         user_id:Int,
       @Field("restaurant_name") restaurant_name:String,
       @Field("restaurant_id")   restaurant_id:Int,
       @Field("umumiy_narx")     umumiy_narx:String,
       @Field("ovqat_narxi")     ovqat_narxi:String,
       @Field("yetkazish_narxi") yetkazish_narxi:String,
       @Field("ovqatlar")        ovqatlar:String,
       @Field("manzil")          manzil:String,
       @Field("qoshimcha_raqam") qoshimcha_raqam:String,
       @Field("yetkazish_vaqt")  yetkazish_vaqt:String
     ):Response<Orders>


    @GET("food_order/getUnconfirmedOrder.php")
    suspend fun getUnconfirmed(
        @Query("user_id") id:Int,
        @Query("confirm_id") confirm_id:Int
    ):Response<List<Orders>>

    @GET("food_order/searchFood.php")
    suspend fun searchFood(
        @Query("key") key:String
    ):Response<List<Food>>

    @GET("food_order/gettypeoffood.php")
    suspend fun getFoodType():Response<List<FoodType>>
}