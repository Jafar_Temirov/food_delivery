package com.example.myapplication.extension

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import java.util.*

fun Fragment.toastSH(message: String) =
    Toast.makeText(context, message, Toast.LENGTH_SHORT).show()

fun Fragment.toastLG(message: String)=
    Toast.makeText(context, message, Toast.LENGTH_LONG).show()

fun Fragment.showDatePickerDialog(callBack: (calendar: Calendar) -> Unit) {
    val calendar = Calendar.getInstance()
    val dateChooserListener = DatePickerDialog.OnDateSetListener { _, year, month, day ->
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, month)
        calendar.set(Calendar.DAY_OF_MONTH, day)
        callBack(calendar)
    }


    DatePickerDialog(
        requireContext(),
        dateChooserListener,
        calendar.get(Calendar.YEAR),
        calendar.get(Calendar.MONTH),
        calendar.get(Calendar.DAY_OF_MONTH)
    ).apply {
        datePicker.minDate = calendar.timeInMillis
        show()
    }

}

fun Fragment.showTimePickerDialog(chosenDate: Calendar?, callBack: (calendar: Calendar?) -> Unit) {
    val calendar = Calendar.getInstance()

    val timeChooserListener = TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
        chosenDate?.set(Calendar.HOUR_OF_DAY, hourOfDay)
        chosenDate?.set(Calendar.MINUTE, minute)
        callBack(chosenDate)
    }

    TimePickerDialog(
        requireContext(),
        timeChooserListener,
        calendar.get(Calendar.HOUR_OF_DAY),
        calendar.get(Calendar.MINUTE),
        true
    ).show()
}



fun View.showSnackBar(message: String) {
    Snackbar.make(this, message, Snackbar.LENGTH_LONG)
        .apply {
            setAction("Ok") { this.dismiss() }.show()
        }
}

fun View?.hideSoftKeyboard() {
    val imm = this?.context?.getSystemService(INPUT_METHOD_SERVICE) as? InputMethodManager
    imm?.hideSoftInputFromWindow(this?.windowToken, 0)
}

fun View.showKeyBord() {
    this.requestFocus()
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
}

fun View.showKeyBoardImplicit() {
    this.requestFocus()
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0)
}

// visibility group

fun View.show() {
    this.visibility = (View.VISIBLE)
}

fun View.showIf(condition: Boolean) {
    if (condition) this.visibility = (View.VISIBLE)
}

fun View.manageVisibility(condition: Boolean) {
    if (condition) this.visibility = (View.VISIBLE)
    else visibility = View.GONE
}

fun View.hide() {
    this.visibility = (View.GONE)
}

fun View.invisible() {
    this.visibility = (View.INVISIBLE)
}

fun View.isVisible(): Boolean {
    return this.visibility == (View.VISIBLE)
}

fun View.isNotVisible(): Boolean {
    return this.visibility != (View.VISIBLE)
}