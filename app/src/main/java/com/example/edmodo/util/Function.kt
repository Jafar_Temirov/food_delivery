package com.example.edmodo.util

import android.app.AlertDialog
import android.app.Application
import android.content.ContentValues
import android.content.Context
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.provider.ContactsContract
import android.util.Log
import android.widget.RelativeLayout
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import java.text.DecimalFormat


class Function {

    companion object{

        fun formatlash(raqam:Int): Number? {
            val formatter = DecimalFormat("#,###,###")
            return formatter.parse(raqam.toString())
        }

        fun Formatlash(raqam: Int):String?{
            val formatter = DecimalFormat("#,###,###")
            return formatter.format(raqam)
        }


        fun alertDialog(context: Context,title:String,message:String){
            val addDialog=AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Xa"){ dialogInterface, i ->
                      Toast.makeText(context,"Yes you can",Toast.LENGTH_SHORT).show()
                }
                .setNegativeButton("Yo'q"){dialogInterface, i ->
                    Toast.makeText(context,"No you can not",Toast.LENGTH_SHORT).show()
                }.create().show()
        }

        fun showToast(context: Context,message: String){
            Toast.makeText(context,message,Toast.LENGTH_SHORT).show()
        }

        fun hasInternetConnection(application: Application):Boolean{
            val connectivityManager=application.getSystemService(
                Context.CONNECTIVITY_SERVICE
            ) as ConnectivityManager
            if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.M){
                Log.d(ContentValues.TAG,"SDK_M")
                val activeNetwork=connectivityManager.activeNetwork?:return false
                val capabilities=connectivityManager.getNetworkCapabilities(activeNetwork)?:return false
                return when{
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)->true
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)->true
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)->true
                    else ->false
                }
            }else {
                Log.d(ContentValues.TAG,"SDK_N")
                connectivityManager.activeNetworkInfo?.run {
                    return when(type) {
                        ConnectivityManager.TYPE_WIFI -> true
                        ContactsContract.CommonDataKinds.Email.TYPE_MOBILE -> true
                        ConnectivityManager.TYPE_ETHERNET -> true
                        else -> false
                    }
                }
            }
            return false
        }

        fun snackbar(
            relativeLayout: RelativeLayout?,
            snack_text: String?
        ) {
            val snackbar: Snackbar = Snackbar
                .make(relativeLayout!!, snack_text!!, Snackbar.LENGTH_INDEFINITE)
                .setActionTextColor(Color.MAGENTA)
                .setDuration(5000)
            snackbar.show()
        }
    }
}