package com.example.edmodo.util

class Constants {
    companion object{
        const val API_KEY="ef9546f6c46043a697d2bc298f12329d"
        const val URL="https://jafartemirov.uz/food/"

        // Location
        const val LONGTITUDE="longtitude"
        const val LATITUDE="latitude"
        const val USER_ID="user_id"
        const val MANZIL="manzil"

        //  Food
       const val MONEY="money"
       const val COUNTER="counter"
       const val EXIST_FOOD="exist_food"
       //
       const val RESTAURANT_ID="restaurant_id"
       const val RESTAURANT_IMAGE_URI="restaurant_uri"
       const val RESTAURANT_NAME="restaurant_name"
       const val DISTANCE="distance"

       // Restaurant
        const val RESTAURANT_ID_REAL="restaurantreal_id"
        const val RESTAURANT_IMAGE_URI_REAL="restaurantreal_uri"
        const val RESTAURANT_NAME_REAL="restaurantreal_name"
        const val DISTANCE_REAL="distance_real"

        // Splash
        const val ISFIRST="is_first"
        const val PERMISSIONS_REQUEST_ENABLE_GPS=9002
    }
}