package com.example.edmodo.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.edmodo.model.Orders
import com.example.edmodo.repository.OrderRepository
import com.example.edmodo.util.Function
import com.example.edmodo.util.Resource
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException

class OrderViewModel(val app:Application,val repository: OrderRepository)
    :AndroidViewModel(app){

    val orders: MutableLiveData<Resource<List<Orders>>> = MutableLiveData()

    fun getUnconfirmed(user_id: Int,confirm_id:Int)=viewModelScope.launch {
        safeUnconfirmedCall(user_id,confirm_id)
    }

    private suspend fun safeUnconfirmedCall(user_id:Int,confirm_id:Int){
        orders.postValue(Resource.Loading())
        try {
            if (Function.hasInternetConnection(app)){
                val response=repository.getUnOrder(user_id,confirm_id)
                orders.postValue(handleUnconfirmedResponse(response))
            }else{
                orders.postValue(Resource.Error("No Internet Connection "))
            }
        }catch (t:Throwable){
            when(t){
                is IOException ->orders.postValue(Resource.Error("Network Failure $t"))
                else -> orders.postValue(Resource.Error("Conversion error $t"))
            }
        }
    }

    private fun handleUnconfirmedResponse(response: Response<List<Orders>>):Resource<List<Orders>>{
        if (response.isSuccessful){
            response.body()?.let {resultResponse->
                return Resource.Success(resultResponse)
            }
        }
        return Resource.Error(response.message())
    }

}