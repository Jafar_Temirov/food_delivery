package com.example.edmodo.viewModel.sharedViewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SharedViewModel(val text:MutableLiveData<CharSequence>): ViewModel() {

    fun setText(input :CharSequence){
        text.postValue(input)
    }

    fun getText():LiveData<CharSequence>{
        return text
    }
}