package com.example.edmodo.viewModel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.edmodo.repository.OrderRepository

class OrderViewModelFactory(
    val app: Application,
    val repository: OrderRepository
) :ViewModelProvider.Factory{

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return OrderViewModel(app,repository) as T
    }
}