package com.example.edmodo.viewModel.sharedViewModel

import android.os.Handler
import androidx.lifecycle.ViewModel
import com.example.edmodo.database.entity.FoodEntity

class SimpleViewModel : ViewModel() {

    private var againAddFunction: (() -> Unit)? = null
    private var runnable: Runnable? = null
    private val myHandler by lazy { Handler() }

    fun deleteMark(foodEntity: FoodEntity) {

        //delete function

        runnable?.let { myHandler.removeCallbacks(it) }
        againAddFunction = {addProduct(foodEntity = foodEntity)}

        runnable = Runnable { againAddFunction = null }
        myHandler.postDelayed(runnable!!, 5000)
    }

    fun addProduct(foodEntity: FoodEntity) {

    }

    fun returnProduct() {
        againAddFunction?.invoke()
    }

}