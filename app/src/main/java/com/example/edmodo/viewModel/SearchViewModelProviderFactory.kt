package com.example.edmodo.viewModel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.edmodo.repository.SearchRepository

class SearchViewModelProviderFactory(
    val app:Application,
    val searchRepository: SearchRepository
) :ViewModelProvider.Factory{

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SearchViewModel(app,searchRepository) as T
    }
}