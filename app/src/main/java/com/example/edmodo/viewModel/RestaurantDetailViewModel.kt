package com.example.edmodo.viewModel
import android.app.Application
import android.content.ContentValues
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.edmodo.model.Food
import com.example.edmodo.model.Orders
import com.example.edmodo.model.RestaurantMenu
import com.example.edmodo.model.User
import com.example.edmodo.repository.RestaurantsRepository
import com.example.edmodo.util.Function
import com.example.edmodo.util.Resource
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException
class RestaurantDetailViewModel(val app: Application,val repository: RestaurantsRepository)
    :AndroidViewModel(app) {

    val menus:MutableLiveData<Resource<List<RestaurantMenu>>> = MutableLiveData()
    val foods:MutableLiveData<Resource<List<Food>>> = MutableLiveData()
    val user:MutableLiveData<Resource<User>> = MutableLiveData()
    val updateLocation:MutableLiveData<Resource<User>> =MutableLiveData()
    val orders:MutableLiveData<Resource<Orders>> = MutableLiveData()

    init {Log.d("RestDetail ViewModel ","initialized")}

    fun getMenu(id:Int)=viewModelScope.launch {safeResultCall(id)}
    fun getFoods(id:Int)=viewModelScope.launch {safeResultCallNote(id)}
    fun SaveUsers(u:String,s:String,l:String,t:String,lat: String,lng:String)=viewModelScope.launch {safeSaveUser(u,s,l,t,lat,lng)}
    fun UpdateLocation(lat:String,long: String,user_id:Int,manzil: String)=viewModelScope.launch {safeUpdateLocation(lat,long,user_id,manzil)}
    fun SaveOrders(user_ids:Int,restaurant_names:String,
                   restaurant_id:Int,
                   umumiy_narx:String,
                   ovqat_narxi:String,
                   yetkazish_narxi:String,
                   ovqatlar:String,
                   manzil:String,
                   qoshimcha_raqam:String,
                   yetkazish_vaqt:String)=viewModelScope.launch {
        safeResultOrders(user_ids,restaurant_names,restaurant_id,umumiy_narx,ovqat_narxi,yetkazish_narxi,ovqatlar,manzil,qoshimcha_raqam,yetkazish_vaqt)
    }

    private suspend fun safeSaveUser(u:String,s:String,l:String,t:String,lat:String,long:String){
        user.postValue(Resource.Loading())
        try {
            if (Function.hasInternetConnection(app)){
                Log.d(ContentValues.TAG,"Loading: Has Internet")
            val response=repository.saveUser(u,s,l,t,lat,long)
            user.postValue(handleSaveUser(response))
            }else {
                user.postValue(Resource.Error("No internet connection"))
                Log.d(ContentValues.TAG,"Loading: No Internet")
            }
        }catch (t:Throwable){
            when(t){
                is IOException->user.postValue(Resource.Error("Network Failure $t"))
                else->user.postValue(Resource.Error("Differenet Error $t"))
            }
        }
    }
    private fun handleSaveUser(response:Response<User>):Resource<User>{
        if (response.isSuccessful){
            response.body()?.let { resultResponse->
                return Resource.Success(resultResponse)
            }
        }
        return Resource.Error(response.message())
    }

    private suspend fun safeResultCall(id: Int) {
        menus.postValue(Resource.Loading())
        try {
           val response=repository.getMenu(id)
           menus.postValue(handleListOfMenus(response))
        }catch (t:Throwable){
            when(t){
                is IOException->menus.postValue(Resource.Error("Network Failure $t"))
                else->menus.postValue(Resource.Error("Conversion error $t"))
            }
        }
    }
    private fun handleListOfMenus(response:Response<List<RestaurantMenu>>)
            :Resource<List<RestaurantMenu>>{
        if (response.isSuccessful){
            response.body()?.let { resultResopnse->
                return Resource.Success(resultResopnse)
            }
        }
        return Resource.Error(response.message())
    }

    private suspend fun safeResultCallNote(id: Int) {
        foods.postValue(Resource.Loading())
        try {
            val response=repository.getFood(id)
            foods.postValue(handleListOfFoods(response))
        }catch (t:Throwable){
            when(t){
                is IOException->foods.postValue(Resource.Error("Network Failure $t"))
                else->foods.postValue(Resource.Error("Conversion error $t"))
            }
        }
    }
    private fun handleListOfFoods(response:Response<List<Food>>)
            :Resource<List<Food>>{
        if (response.isSuccessful){
            response.body()?.let { resultResopnse->
                return Resource.Success(resultResopnse)
            }
        }
        return Resource.Error(response.message())
    }

    private suspend fun safeUpdateLocation(lat:String,long:String,userid:Int,manzil:String){
        updateLocation.postValue(Resource.Loading())
        try {
            val response=repository.UpdateLocation(lat,long,userid,manzil)
            updateLocation.postValue(handleLocation(response))
        }catch (t:Throwable){
            when(t){
                is IOException->updateLocation.postValue(Resource.Error("Network Failure $t"))
                else->updateLocation.postValue(Resource.Error("Conversion error $t"))
            }
        }
    }
    private fun handleLocation(response: Response<User>):Resource<User>{
        if (response.isSuccessful){
            response.body()?.let { resultCode->
                return Resource.Success(resultCode)
            }
        }
        return Resource.Error(response.message())
    }

    private suspend fun safeResultOrders(user_id:Int,
                                         restaurant_name:String,
                                         restaurant_id:Int,
                                         umumiy_narx:String,
                                         ovqat_narxi:String,
                                         yetkazish_narxi:String,
                                         ovqatlar:String,
                                         manzil:String,
                                         qoshimcha_raqam:String,
                                         yetkazish_vaqt:String){
      orders.postValue(Resource.Loading())
        try {
            val response=repository.SaveOrders(user_id,
                restaurant_name,
                restaurant_id,umumiy_narx,ovqat_narxi,yetkazish_narxi,ovqatlar,manzil,qoshimcha_raqam,yetkazish_vaqt
                )
            orders.postValue(handleOrders(response))
        }catch (t:Throwable){
            when(t){
                is IOException->orders.postValue(Resource.Error("Network Failure $t"))
                else->orders.postValue(Resource.Error("Conversion error $t"))
            }
        }
    }
    private fun handleOrders(response:Response<Orders>):Resource<Orders>{
        if (response.isSuccessful){
            response.body()?.let { resultCode->
                return Resource.Success(resultCode)
            }
        }
        return Resource.Error(response.message())
    }


    // Database
    fun deleteAllOreders()=viewModelScope.launch { repository.deleteAll()}

}
