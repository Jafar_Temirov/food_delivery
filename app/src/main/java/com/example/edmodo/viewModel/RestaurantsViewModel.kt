package com.example.edmodo.viewModel
import android.app.Application
import android.content.ContentValues.TAG
import android.content.Context
import android.net.ConnectivityManager
import android.net.ConnectivityManager.TYPE_ETHERNET
import android.net.ConnectivityManager.TYPE_WIFI
import android.net.NetworkCapabilities
import android.os.Build
import android.provider.ContactsContract.CommonDataKinds.Email.TYPE_MOBILE
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.edmodo.RestaurantApplication
import com.example.edmodo.database.entity.FoodEntity
import com.example.edmodo.model.*
import com.example.edmodo.repository.RestaurantsRepository
import com.example.edmodo.util.Function
import com.example.edmodo.util.Resource
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException

class RestaurantsViewModel(val app :Application, val restaurantsRepository: RestaurantsRepository)
    : AndroidViewModel(app) {

    val  restaurant:MutableLiveData<Resource<List<Restaurants>>> = MutableLiveData()
    val text:MutableLiveData<CharSequence> =MutableLiveData()


    fun setText(input :CharSequence){
        text.postValue(input)
    }
    fun getText(): LiveData<CharSequence> {
        return text
    }

    fun getRestaurantsList()=viewModelScope.launch { safeRestaurantCall() }

    // Database
    fun getOrders()=restaurantsRepository.getFoodsDb()

    fun deleteOrders(foods: FoodEntity)=viewModelScope.launch {
        restaurantsRepository.deleteFood(foods)
    }
    fun getSummOfFood()=restaurantsRepository.getMoney()
    
    fun deleteAllOreders()=viewModelScope.launch { restaurantsRepository.deleteAll()}

    fun InsertFoods(food: FoodEntity)=viewModelScope.launch {
        restaurantsRepository.insert(food)
    }

    fun Comparing(dd:Int)= restaurantsRepository.comparing(dd)

    fun UpdateCount(counter:Int,food_id:Int)=viewModelScope.launch {
        restaurantsRepository.UpdateCounter(counter,food_id)
    }
    fun updateDefault(food: FoodEntity)=viewModelScope.launch {
        restaurantsRepository.updateDefault(food) }


    private suspend fun safeRestaurantCall(){
        restaurant.postValue(Resource.Loading())
        try {
            if (Function.hasInternetConnection(app)){
                Log.d(TAG,"Loading: Has Internet")
                val response=restaurantsRepository.getAllRestaurants()
                restaurant.postValue(handleRestaurantsListReposnse(response))
            }else {
                restaurant.postValue(Resource.Error("No internet connection"))
                Log.d(TAG,"Loading: No Internet")
            }
        }catch (t:Throwable){
            when(t){
                is IOException->restaurant.postValue(Resource.Error("Network Failure $t"))
                else -> restaurant.postValue(Resource.Error("Conversion error  $t"))
            }
        }
    }
    private fun handleRestaurantsListReposnse(response:Response<List<Restaurants>>):Resource<List<Restaurants>>{
        if (response.isSuccessful){
            response.body()?.let {resultResponse->
                return Resource.Success(resultResponse)
            }
        }

        return Resource.Error(response.message())
    }

}