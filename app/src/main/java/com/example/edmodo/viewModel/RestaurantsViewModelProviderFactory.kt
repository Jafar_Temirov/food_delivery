package com.example.edmodo.viewModel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.edmodo.repository.RestaurantsRepository

@Suppress("UNCHECKED_CAST")
class RestaurantsViewModelProviderFactory(
    val app:Application,
    val restaurantsRepository: RestaurantsRepository
) :ViewModelProvider.Factory{

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return RestaurantsViewModel(app,restaurantsRepository) as T
    }

}