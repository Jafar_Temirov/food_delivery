package com.example.edmodo.viewModel

import android.app.Application
import android.content.ContentValues
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.edmodo.model.Food
import com.example.edmodo.model.FoodType
import com.example.edmodo.repository.SearchRepository
import com.example.edmodo.util.Function
import com.example.edmodo.util.Resource
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException

class SearchViewModel(val app:Application,val repository: SearchRepository)
    :AndroidViewModel(app){
    val search: MutableLiveData<Resource<List<Food>>> = MutableLiveData()
    val foodType:MutableLiveData<Resource<List<FoodType>>> = MutableLiveData()

    fun searchFoods(key:String)=viewModelScope.launch{safeSearchCall(key)}
    fun getFoodType()=viewModelScope.launch {safeFoodType()}

    private suspend fun safeSearchCall(key:String){
        search.postValue(Resource.Loading())
        try {
            if (Function.hasInternetConnection(app)){
                val response=repository.seachFood(key)
                search.postValue(handleSearch(response))
            }else{
                search.postValue(Resource.Error("No Internet Connection"))
            }
        }catch (t:Throwable){
            when(t){
                is IOException->search.postValue(Resource.Error("Network Failure $t"))
                else -> search.postValue(Resource.Error("Conversion error $t"))
            }
        }
    }
    private fun handleSearch(response: Response<List<Food>>):Resource<List<Food>>{
        if (response.isSuccessful){
            response.body()?.let { resultCode->
                return Resource.Success(resultCode)
            }
        }
        return Resource.Error(response.message())
    }
    private suspend fun safeFoodType() {
        foodType.postValue(Resource.Loading())
        try {
            if(Function.hasInternetConnection(app)){
                val response=repository.getFoodType()
                foodType.postValue(handleFoodTypeCall(response))
            }else {
                foodType.postValue(Resource.Error("No Internet Connection"))
            }
        } catch (t: Throwable) {
            when (t) {
                is IOException -> foodType.postValue(Resource.Error("Network Failure $t"))
                else -> foodType.postValue(Resource.Error("Conversion error $t"))
            }
        }
    }
    private fun handleFoodTypeCall(response: Response<List<FoodType>>):Resource<List<FoodType>>{
        if (response.isSuccessful){
            response.body()?.let { resultCode->
                return Resource.Success(resultCode)
            }
        }
        return Resource.Error(response.message())
    }

}