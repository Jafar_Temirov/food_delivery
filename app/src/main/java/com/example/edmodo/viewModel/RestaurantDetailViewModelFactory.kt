package com.example.edmodo.viewModel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.edmodo.repository.RestaurantsRepository

class RestaurantDetailViewModelFactory(
    val app:Application,
    val repository: RestaurantsRepository
):ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
      return RestaurantDetailViewModel(app,repository) as T
    }

}