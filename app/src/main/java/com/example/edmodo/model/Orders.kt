package com.example.edmodo.model
import java.io.Serializable

data class Orders(
             var user_id:Int,
             var restaurant_name:String,
             var restaurant_id:Int,
             var umumiy_narx:String,
             var ovqat_narxi:String,
             var yetkazish_narxi:String,
             var ovqatlar:String,
             var manzil:String,
             var qoshimcha_raqam:String,
             var yetkazish_vaqt:String,
             var message:String,
             var success:Boolean)