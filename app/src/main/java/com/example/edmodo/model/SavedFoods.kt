package com.example.edmodo.model

import java.io.Serializable

data class SavedFoods(
    var food_id: Int,
    var count: Int,
    var food_name:String) :Serializable{
}