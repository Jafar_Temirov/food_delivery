package com.example.edmodo.model

data class RestaurantMenu(
    var id:Int,
    var menu:String,
    var rest_id:String)