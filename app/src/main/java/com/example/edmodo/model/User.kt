package com.example.edmodo.model

data class User(
    var username:String,
    var surname:String,
    var location:String,
    var tel_number:String,
    var success:Boolean,
    var message:String,
    var user_id:Int
)