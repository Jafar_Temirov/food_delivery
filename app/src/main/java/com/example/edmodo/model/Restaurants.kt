package com.example.edmodo.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Restaurants(
    var id:Int?=null,
    var name:String,
    var more_info:String,
    var latitude:String,
    var longtitude:String,
    var logo_image:String,
    var main_image:String,
    var rating:String
) :Serializable{
    override fun equals(other: Any?): Boolean {
        if (javaClass!=other?.javaClass){
            return false
        }
        other as Restaurants
        if (id!=other.id){
            return false
        }
        if (name!=other.name){
            return false
        }
        if (more_info!=other.more_info){
            return false
        }
        if (latitude!=other.latitude){
            return false
        }
        if (longtitude!=other.longtitude){
            return false
        }
        if (main_image!=other.main_image){
            return false
        }
        if (rating!=other.rating){
            return false
        }

        return true
    }
}