package com.example.edmodo.model

data class FoodType(
    var id:Int,
    var food_title:String,
    var food_start:Float,
    var food_description:String,
    var food_image:String
)