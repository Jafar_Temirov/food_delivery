package com.example.edmodo.model

import java.io.Serializable
import kotlin.math.cos

data class Food(
    var id:Int,
    var food_title:String,
    var food_description:String,
    var cost:String,
    var food_image:String,
    var restaurant_menu_id:String,
    var success:Boolean,
    var message:String
) :Serializable{
    override fun equals(other: Any?): Boolean {
        if (javaClass != other?.javaClass) {
            return false
        }
        other as Food
        if (id!=other.id){
            return false
        }
        if (food_title!=other.food_title){
            return false
        }
        if (food_description!=other.food_description){
            return false
        }
        if(cost!=other.cost){
            return false
        }
        if (food_image!=other.food_image){
            return false
        }
        if (restaurant_menu_id!=other.restaurant_menu_id){
            return false
        }
        if (success!=other.success){
            return false
        }
        if (message!=other.message){
            return false
        }
        return true
    }
}